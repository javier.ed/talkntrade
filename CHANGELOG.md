# CHANGELOG

## [0.4.0] - 2019-10-31

### Added

* Feature to rate purchase orders !85 !89
* Feature to report users !83 !86 !88
* Feature to rate products !80 !84 !87
* Feature to block and unblock users !76
* Merge request templates !74
* Issue templates !73

### Changed

* Upgrade dependencies !82
* Call `attr_readonly` in almost all models !78
* Ruby on Rails upgraded to version 6.0 !75 !77

### Fixed

* Methods to find talks in `User` model !81
* Typo in `Mutations::PayOrder` fixed !79


## [0.3.0] - 2019-09-30

### Added

* TX URL field in transaction type !71 !72
* Scopes for client apps !68
* lookahead of graphql-ruby to check which child fields are selected in TalkQuery !67
* HomeController#index to serve GET / request !66
* QueuesService !65 !61 !60 !53
* Webmock for Bitcoin RPC tests !64
* More validations in models Product, User & User::Profile !63 !55
* Feature to disable users !62
* Sidekiq for background jobs !59
* Sent message field in send message mutation !58
* Filter variables parameter in production mode !57
* GNU AGPLv3 LICENSE !56
* Editable fees !54
* Some DB indexes in models Product & User !51
* Validations in Talk model !50

### Changed

* Upgrade graphql-ruby to 1.9.12 !70
* Cache-Control HTTP response header added in UploadsController !49
* Upgrade dependencies !47

### Fixed

* Update assets balances atomically after transactions !52
* Only find talks from the current user in TalkQuery !48

### Removed

* schema.graphql !69


## [0.2.0] - 2019-09-02

### Added

* GraphQL mutation to pay order !45
* GraphQL mutation to remove item from purchase order !44
* GraphQL mutation to add item to purchase order !43
* GraphQL query to show user !42
* GraphQL mutation to finish sales !41
* GraphQL mutation to start sales !40
* Added current sale field to product type !39
* User can now create sales of their products !38
* Users can now edit their products !37
* Adding GraphQL queries for products !36
* Users can create products now !34
* Update & show profile !32

### Changed

* Improve background job for transactions !46
* Adding support for Apollo Client's query batching !35
* Upgrade dependencies !33
* Disable Rails/DynamicFindBy in Rubocop !31
* Client apps can expire now !30
* Users query improvements !29


## [0.1.0] - 2019-07-01

### Added

* Reset password feature !28
* Email settings !27
* Change password feature !25
* Mongoid history tracker added !24
* Info query !22
* Set response locale from request headers !21
* Enable Mongoid Query Cache & some other improvements to allow Apollo clients to handle cached queries !20
* Talk query with all unseen messages !19
* GraphQL mutation to mark messages as seen !18
* Allow GraphQL introspection in development environment !17
* Talks query improvements !16

### Changed

* Upgrade dependencies !26

### Fixed

* Fix typo !23


## [0.0.0] - 2019-05-27

* Fix some deployment issues !13
* Security improvements !12
* BTC transfers in messages !11
* BTC deposits & withdrawals !9 !10
* Wallets feature !8
* Limit messages to enable infinite scrolling in talks !7
* Talks feature !6
* Adjustments for devices connections !5
* Devices id & secret key !4
* Login & logout !3
* User registration !2
* Client app id & secret key !1
* All is new
