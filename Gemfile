# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '~> 2.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0', '>= 6.0.2.2'
# Use Puma as the app server
gem 'puma', '~> 4.3', '>= 4.3.3'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.8'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.1', '>= 4.1.3'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1', '>= 3.1.13'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '~> 1.4', '>= 1.4.6', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', '~> 1.1', '>= 1.1.1'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_bot_rails', '~> 5.1', '>= 5.1.1'
  gem 'rspec-rails', '~> 4.0'
  gem 'rubocop', '~> 0.80.1'
  gem 'rubocop-performance', '~> 1.5', '>= 1.5.2'
  gem 'rubocop-rails', '~> 2.5'
  gem 'rubocop-rspec', '~> 1.38', '>= 1.38.1'
end

group :development do
  gem 'listen', '~> 3.2', '>= 3.2.1'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '~> 2.1'
  gem 'spring-watcher-listen', '~> 2.0.1'
end

group :test do
  gem 'database_cleaner', '~> 1.8', '>= 1.8.3'
  gem 'email_spec', '~> 2.2'
  gem 'mongoid-rspec', '~> 4.0', '>= 4.0.1'
  gem 'rails-controller-testing', '~> 1.0', '>= 1.0.4'
  gem 'shoulda-matchers', '~> 4.3'
  gem 'simplecov', '~> 0.18.5', require: false
  gem 'webmock', '~> 3.8', '>= 3.8.3'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# Database management
gem 'mongoid', '~> 7.0', '>= 7.0.6'
gem 'mongoid-autoinc', '~> 6.0', '>= 6.0.3'
gem 'mongoid-history', '~> 0.8.2'
gem 'mongoid_rails_migrations', '~> 1.2', '>= 1.2.1'

# File management
gem 'carrierwave', '~> 2.1'
gem 'carrierwave-mongoid', '~> 1.3', require: 'carrierwave/mongoid'
gem 'file_validators', '~> 3.0.0.beta1'
gem 'mini_magick', '~> 4.10', '>= 4.10.1'
gem 'terrapin', '~> 0.6.0'

# Authentication
gem 'jwt', '~> 2.2', '>= 2.2.1'

# Localization
gem 'countries', '~> 3.0', '>= 3.0.1'
gem 'rails-i18n', '~> 6.0'
gem 'timeliness-i18n', '~> 0.9.0'

# GraphQL
gem 'apollo_upload_server', '~> 2.0', '>= 2.0.1'
gem 'graphql', '~> 1.10', '>= 1.10.5'

# Money
gem 'money-rails', '~> 1.13', '>= 1.13.3'

# Validators
gem 'validates_timeliness', '~> 5.0.0.beta1'

# Background jobs
gem 'sidekiq', '~> 6.0', '>= 6.0.6'
gem 'sidekiq-cron', '~> 1.1'

# JSON
gem 'json', '~> 2.3'
