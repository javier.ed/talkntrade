# Talk n Trade (Server)

[![pipeline status](https://gitlab.com/talkntrade/talkntrade/badges/master/pipeline.svg)](https://gitlab.com/talkntrade/talkntrade/commits/master)

[![coverage report](https://gitlab.com/talkntrade/talkntrade/badges/master/coverage.svg)](https://gitlab.com/talkntrade/talkntrade/commits/master)

## Requirements

* Git 2.20+
* Ruby 2.5
* MongoDB 4.0
* Redis 5.0+

## Basic configuration

```
bundle install
cp config/cable.example.yml config/cable.yml
cp config/mongoid.example.yml config/mongoid.yml
cp config/settings.example.yml config/settings.yml
```

## Deployment for developers

```
rails s -b 0.0.0.0
```

### Running tests

```
rspec
```
