# frozen_string_literal: true

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :http_request

    def connect
      self.http_request = request
    end
  end
end
