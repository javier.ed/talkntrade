# frozen_string_literal: true

class ApplicationController < ActionController::API
  def require_device_authorization!
    render(json: { message: 'Unauthorized' }, status: :unauthorized) unless authorization.device_authorized?
  end

  def authenticate_user!
    render(json: { message: 'Unauthenticated' }, status: :unauthorized) unless authorization.user_authenticated?
  end

  delegate :current_user, to: :authorization

  private

  def authorization
    return @authorization if @authorization

    @authorization = Talkntrade::Authorization.new request: request
  end
end
