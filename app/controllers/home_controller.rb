# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    render json: {
      name: Settings.name,
      description: Settings.description,
      version: Talkntrade::VERSION,
      environment: Rails.env
    }
  end
end
