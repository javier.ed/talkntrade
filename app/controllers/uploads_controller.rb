# frozen_string_literal: true

class UploadsController < ApplicationController
  before_action :require_device_authorization!, only: :user_avatar_image

  # GET /uploads/users/:id/avatar_image
  def user_avatar_image
    user = User.find params[:id]
    avatar_image = if params[:version] && user.profile.avatar_image.versions[params[:version].to_sym]
                     user.profile.avatar_image.versions[params[:version].to_sym]
                   else
                     user.profile.avatar_image
                   end

    render_image(avatar_image.read, avatar_image.file&.content_type, user.profile.updated_at)
  rescue Mongoid::Errors::DocumentNotFound
    file_not_found
  end

  private

  def file_not_found
    render json: { message: 'File not found' }, status: :not_found
  end

  # Render an image file
  #
  # @param content [String]
  # @param content_type [String]
  # @param last_modified [Time]
  def render_image(content, content_type, last_modified)
    if content && content_type && last_modified && stale?(etag: content, last_modified: last_modified, public: true)
      send_data content, type: content_type, disposition: 'inline'
      expires_in 1.hour, public: true
    else
      file_not_found
    end
  end
end
