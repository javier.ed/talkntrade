# frozen_string_literal: true

module Contexts
  class CustomContext < GraphQL::Query::Context
    def initialize(query:, values:, object:)
      super

      self[:current_user_id] = authorization.current_user&.id if self[:channel]
    end

    def request
      self[:request]
    end

    def introspection?
      self[:introspection]
    end

    # To get the current device
    #
    # @return [Device, nil] Return the current device or nil
    def current_device # rubocop:disable Rails/Delegate
      authorization.current_device
    end

    # To get the current session
    #
    # @return [Device::Session, nil] Return the current session or nil
    def current_session # rubocop:disable Rails/Delegate
      authorization.current_session
    end

    # To get the current user
    #
    # @return [User, nil] Return the current user or nil
    def current_user # rubocop:disable Rails/Delegate
      authorization.current_user
    end

    private

    # Initialize an instance of `Talkntrade::Authorization`
    #
    # @return [Talkntrade::Authorization] Authorization object
    def authorization
      return @authorization if @authorization

      @authorization = Talkntrade::Authorization.new request: request
    end
  end
end
