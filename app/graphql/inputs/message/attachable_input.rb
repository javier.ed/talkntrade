# frozen_string_literal: true

require 'talkntrade/graphql'

module Inputs
  module Message
    class AttachableInput < Inputs::BaseInput
      include Talkntrade::Graphql::AuthenticateUser
      include Talkntrade::Graphql::Scopes

      graphql_name 'MessageAttachableInput'

      argument :type, String, required: false, default_value: 'transaction'
      argument :amount, Float, required: false
      argument :currency_code, String, required: false, default_value: 'BTC'
    end
  end
end
