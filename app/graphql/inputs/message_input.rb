# frozen_string_literal: true

require 'talkntrade/graphql'

module Inputs
  class MessageInput < Inputs::BaseInput
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    argument :content, String, required: true
    argument :attachable, Inputs::Message::AttachableInput, required: false
    argument :talk_id, ID, required: true
  end
end
