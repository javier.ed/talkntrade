# frozen_string_literal: true

require 'talkntrade/graphql'

module Inputs
  class OtpInput < Inputs::BaseInput
    include Talkntrade::Graphql::RequireDeviceAuthorization
    include Talkntrade::Graphql::Scopes

    argument :id, ID, required: true
    argument :password, String, required: true
  end
end
