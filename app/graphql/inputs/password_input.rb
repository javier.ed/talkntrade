# frozen_string_literal: true

require 'talkntrade/graphql'

module Inputs
  class PasswordInput < Inputs::BaseInput
    include Talkntrade::Graphql::RequireDeviceAuthorization
    include Talkntrade::Graphql::Scopes

    argument :password, String, required: true
    argument :password_confirmation, String, required: true
  end
end
