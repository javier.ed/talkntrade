# frozen_string_literal: true

require 'talkntrade/graphql'

module Inputs
  class ReportUserInput < Inputs::BaseInput
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    description 'Input data of an user report'

    argument :user_id, ID, 'ID of the user to report', required: true
    argument :category, String, "Category (accepted values: #{Settings.reported_user_categories.join(', ')})",
             required: true
    argument :subject, String, 'Subject', required: true
    argument :message, String, 'Message', required: true
  end
end
