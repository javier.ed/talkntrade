# frozen_string_literal: true

require 'talkntrade/graphql'

module Inputs
  module User
    class ProfileInput < Inputs::BaseInput
      include Talkntrade::Graphql::AuthenticateUser
      include Talkntrade::Graphql::Scopes

      graphql_name 'UserProfileInput'

      argument :display_name, String, required: true
      argument :first_name, String, required: true
      argument :last_name, String, required: true
      argument :birthdate, GraphQL::Types::ISO8601DateTime, required: false
      argument :country_gec, String, required: true
      argument :bio, String, required: true
      argument :avatar_image, ApolloUploadServer::Upload, 'Avatar image file', required: false
      argument :remove_avatar_image, Boolean, 'Remove the current avatar image', required: false
    end
  end
end
