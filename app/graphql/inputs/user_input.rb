# frozen_string_literal: true

require 'talkntrade/graphql'

module Inputs
  class UserInput < Inputs::BaseInput
    include Talkntrade::Graphql::RequireNoAuthentication
    include Talkntrade::Graphql::Scopes

    argument :username, String, required: true
    argument :email, String, required: true
    argument :password, String, required: true
    argument :password_confirmation, String, required: true
  end
end
