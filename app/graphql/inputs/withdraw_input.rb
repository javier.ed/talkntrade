# frozen_string_literal: true

require 'talkntrade/graphql'
module Inputs
  class WithdrawInput < Inputs::BaseInput
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    argument :currency_iso_code, String, required: true
    argument :amount, Float, required: true
    argument :receiver_address, String, required: true
  end
end
