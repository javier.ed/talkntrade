# frozen_string_literal: true

module Mutations
  class BaseMutation < GraphQL::Schema::Mutation
    Mongoid::QueryCache.enabled = false

    field :success, Boolean, null: true
    field :message, String, null: true
    field :errors, [Types::ErrorType], null: true

    delegate :request, :current_device, :current_session, :current_user, to: :context

    def parse_errors(resource, base_path = [], level = 0)
      base_path = base_path.collect { |k| k.to_s.camelize(:lower) }

      errors = resource.errors.map do |attribute, message|
        path = base_path + [attribute.to_s.camelize(:lower)]
        { path: path, message: message }
      end

      if level < 3
        resource.nested_attributes.each do |key, _|
          name = key[0..-12]
          res = resource.send(name)
          path = base_path + [name.camelize(:lower)]

          if res.is_a?(Array)
            i = 0
            res.each do |r|
              errors += parse_errors(r, path + [i], level + 1)
              i += 1
            end
          elsif res
            errors += parse_errors(res, path, level + 1)
          end
        end
      end

      errors
    end
  end
end
