# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class BlockUser < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    description 'Attempt to block an user from the current user'

    argument :user_id, ID, 'ID of the user to block', required: true

    def resolve(user_id:)
      QueuesService.call(action: :block_user, scope: "user_id_#{current_user.id}") do
        resource = current_user.blocked_users.build blocked_user_id: user_id

        if resource.save
          { success: true, message: I18n.t('mutations.block_user.success') }
        else
          { success: false, message: I18n.t('mutations.block_user.fail') }
        end
      end
    end
  end
end
