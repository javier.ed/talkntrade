# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class ChangeEmail < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    argument :password, String, required: true
    argument :email, String, required: true

    def resolve(password:, email:)
      QueuesService.call(action: :update_user, scope: "id_#{current_user.id}") do
        unless current_user.authenticate password
          return error_response [{ path: %i[password], message: I18n.t('errors.messages.invalid') }]
        end

        if current_user.change_email email
          { success: true, message: I18n.t('mutations.change_email.success') }
        else
          error_response parse_errors(current_user)
        end
      end
    end

    private

    def error_response(errors)
      { success: false, message: I18n.t('mutations.change_email.fail'), errors: errors }
    end
  end
end
