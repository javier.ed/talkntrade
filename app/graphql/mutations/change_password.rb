# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class ChangePassword < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    argument :password, String, required: true
    argument :attributes, Inputs::PasswordInput, required: true

    def resolve(password:, attributes:)
      QueuesService.call(action: :update_user, scope: "id_#{current_user.id}") do
        unless current_user.authenticate password
          return error_response [{ path: %i[password], message: I18n.t('errors.messages.invalid') }]
        end

        if current_user.change_password attributes.password, attributes.password_confirmation
          { success: true, message: I18n.t('mutations.change_password.success') }
        else
          error_response parse_errors(current_user, %i[attributes])
        end
      end
    end

    private

    def error_response(errors)
      { success: false, message: I18n.t('mutations.change_password.fail'), errors: errors }
    end
  end
end
