# frozen_string_literal: true

require 'talkntrade/graphql'
require 'talkntrade/fail2ban'

module Mutations
  class Login < Mutations::BaseMutation
    include Talkntrade::Graphql::RequireNoAuthentication
    include Talkntrade::Graphql::Scopes
    include Talkntrade::Fail2ban

    argument :login, String, required: true
    argument :password, String, required: true

    field :user, Types::UserType, null: true
    field :session_id, ID, null: true

    def resolve(login:, password:)
      QueuesService.call(action: :login_or_register, scope: "device_id_#{current_device.id}") do
        user = User.only_enabled.find_by_login(login)

        return error_response(user) unless user&.authenticate(password)

        resource = current_device.sessions.build(user: user)

        if resource.save
          current_device.login_attempts.find_or_create_by(user: user).reset_counter
          { success: true, message: I18n.t('mutations.login.success'), user: user, session_id: resource.id }
        else
          error_response(user)
        end
      rescue Mongoid::Errors::DocumentNotFound
        error_response
      end
    end

    private

    def error_response(user = nil)
      current_device.login_attempts.find_or_create_by(user: user).increment_counter if user
      add_to_fail2ban(request.remote_ip, 'FAILED TO LOGIN')
      { success: false, message: I18n.t('mutations.login.fail') }
    end
  end
end
