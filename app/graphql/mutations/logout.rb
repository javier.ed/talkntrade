# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class Logout < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    def resolve
      QueuesService.call(action: :logout, scope: "user_id_#{current_user.id}") do
        if current_session.update finished_at: Time.current
          { success: true, message: I18n.t('mutations.logout.success') }
        else
          { success: false, message: I18n.t('mutations.logout.fail') }
        end
      end
    end
  end
end
