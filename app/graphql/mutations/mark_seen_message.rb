# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class MarkSeenMessage < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    argument :id, ID, required: true

    def resolve(id:)
      QueuesService.call(action: :mark_seen_message, scope: "user_id_#{current_user.id}_message_id_#{id}") do
        message = Message.find id
        talker = message.talk.talkers.find_by user: current_user
        resource = message.recipients.find_by talker: talker, seen_at: nil

        if resource&.update seen_at: Time.current
          { success: true }
        else
          error_response
        end
      rescue Mongoid::Errors::DocumentNotFound
        error_response
      end
    end

    private

    def error_response
      { success: false, message: I18n.t('mutations.mark_seen_message.fail') }
    end
  end
end
