# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class Register < Mutations::BaseMutation
    include Talkntrade::Graphql::RequireNoAuthentication
    include Talkntrade::Graphql::Scopes

    argument :attributes, Inputs::UserInput, required: true

    field :user, Types::UserType, null: true
    field :session_id, ID, null: true

    def resolve(attributes:)
      QueuesService.call(action: :login_or_register, scope: "device_id_#{current_device.id}") do
        resource = User.new(
          username: attributes.username, email: attributes.email, password: attributes.password,
          password_confirmation: attributes.password_confirmation
        )

        if resource.save
          session = current_device.sessions.create! user: resource

          { success: true, message: I18n.t('mutations.register.success'), user: resource, session_id: session.id }
        else
          { success: false, message: I18n.t('mutations.register.fail'), errors: parse_errors(resource, %w[user]) }
        end
      end
    end
  end
end
