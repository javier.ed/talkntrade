# frozen_string_literal: true

require 'talkntrade/fail2ban'

module Mutations
  class RegisterDevice < Mutations::BaseMutation
    include Talkntrade::Fail2ban

    argument :app_id, ID, required: true
    argument :app_token, String, required: true

    field :device_id, ID, null: true
    field :device_secret_key, String, null: true
    field :app_token, String, 'JWT with response data encoded using the app secret key', null: true

    def self.visible?(context)
      super && !context.current_device
    end

    def self.authorized?(object, context)
      super && !context.current_device
    end

    def resolve(app_id:, app_token:)
      QueuesService.call(action: :register_device, scope: "client_app_id_#{app_id}") do
        client_app = ClientApp.find_unexpired(app_id)
        return error_response unless client_app&.jwt_decode(app_token)

        resource = client_app.devices.new

        if resource.save
          {
            success: true,
            message: I18n.t('mutations.register_device.success'),
            device_id: resource.id,
            device_secret_key: resource.secret_key,
            app_token: client_app.jwt_encode(device_id: resource.id, device_secret_key: resource.secret_key)
          }
        else
          error_response
        end
      rescue JWT::DecodeError
        error_response
      end
    end

    def error_response
      add_to_fail2ban(request.remote_ip, 'FAILED TO REGISTER DEVICE')
      { success: false, message: I18n.t('mutations.register_device.fail') }
    end
  end
end
