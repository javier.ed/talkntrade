# frozen_string_literal: true

module Mutations
  class ReportUser < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    description 'Attempt to report an user'

    argument :attributes, Inputs::ReportUserInput, 'Input data', required: true

    # @param attributes [Inputs::ReportUserInput]
    def resolve(attributes:)
      QueuesService.call action: :report_user, scope: "user_id_#{current_user.id}" do
        resource = current_user.reported_users.build(
          reported_user_id: attributes.user_id,
          category: attributes.category,
          subject: attributes.subject,
          message: attributes.message
        )

        if resource.save
          { success: true, message: I18n.t('mutations.report_user.success') }
        else
          {
            success: false,
            message: I18n.t('mutations.report_user.fail'),
            errors: parse_errors(resource, %w[attributes])
          }
        end
      end
    end
  end
end
