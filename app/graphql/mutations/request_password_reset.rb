# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class RequestPasswordReset < Mutations::BaseMutation
    include Talkntrade::Graphql::RequireNoAuthentication
    include Talkntrade::Graphql::Scopes

    argument :login, String, required: true

    field :otp_id, String, null: true

    def resolve(login:)
      QueuesService.call(action: :login_or_register, scope: "device_id_#{current_device.id}") do
        user = User.only_enabled.find_by_login login
        resource = user&.request_password_reset

        if resource
          { success: true, message: I18n.t('mutations.request_password_reset.success'), otp_id: resource.id }
        else
          error_response
        end
      rescue Mongoid::Errors::DocumentNotFound
        error_response
      end
    end

    private

    def error_response
      { success: false, message: I18n.t('mutations.request_password_reset.fail') }
    end
  end
end
