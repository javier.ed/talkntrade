# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class ResetPassword < Mutations::BaseMutation
    include Talkntrade::Graphql::RequireDeviceAuthorization
    include Talkntrade::Graphql::Scopes

    argument :otp, Inputs::OtpInput, required: true
    argument :attributes, Inputs::PasswordInput, required: true

    def resolve(otp:, attributes:)
      QueuesService.call(action: :login_or_register, scope: "device_id_#{current_device.id}") do
        otp_record = Otp.only_enabled.find_and_authenticate(otp.id, :reset_password, otp.password)

        unless otp_record
          return error_response([{ path: %i[otp password], message: I18n.t('errors.messages.invalid') }])
        end

        resource = otp_record.user

        user_queue = QueuesService.call(action: :update_user, scope: "id_#{resource.id}")

        if resource.change_password attributes.password, attributes.password_confirmation
          otp_record.mark_as_used
          { success: true, message: I18n.t('mutations.reset_password.success') }
        else
          error_response parse_errors(resource, %i[attributes])
        end
      ensure
        user_queue&.finish_turn
      end
    end

    private

    def error_response(errors)
      { success: false, message: I18n.t('mutations.reset_password.fail'), errors: errors }
    end
  end
end
