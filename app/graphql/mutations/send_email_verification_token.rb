# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class SendEmailVerificationToken < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    def resolve
      QueuesService.call(action: :send_email_verification_token, scope: "user_id_#{current_user.id}") do
        if current_user.send_email_verification_token
          { success: true, message: I18n.t('mutations.send_email_verification_token.success') }
        else
          { success: false, message: I18n.t('mutations.send_email_verification_token.fail') }
        end
      end
    end
  end
end
