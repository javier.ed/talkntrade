# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class SendMessage < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    argument :attributes, Inputs::MessageInput, required: true

    field :sent_message, Types::MessageType, null: true

    def resolve(attributes:)
      QueuesService.call(action: :send_message, scope: "user_id_#{current_user.id}") do
        talk = current_user.find_enabled_talk(attributes.talk_id)

        return error_response if talk.nil?

        talker = talk.talkers.find_by(user: current_user)

        resource = Message.new talk: talk, talker: talker, content: attributes.content.strip

        if attributes.attachable.present? && ['"transaction"', 'transaction'].include?(attributes.attachable.type)
          transaction_queue = QueuesService.call(action: :create_transaction, scope: "user_id_#{current_user.id}")

          resource.attachable = new_transaction(
            find_sender(attributes.attachable.currency_code),
            find_receiver(talk, attributes.attachable.currency_code),
            attributes.attachable.amount, attributes.attachable.currency_code
          )
        end

        if resource.save
          talk.talkers.each do |t|
            TalkntradeSchema.subscriptions.trigger('new_message', {}, resource, scope: t.user.id)
          end

          { success: true, sent_message: resource }
        else
          error_response(resource)
        end
      rescue Mongoid::Errors::DocumentNotFound
        error_response
      ensure
        transaction_queue&.finish_turn
      end
    end

    private

    def error_response(resource = nil)
      {
        success: false, message: I18n.t('mutations.send_message.fail'),
        errors: (resource.nil? ? nil : parse_errors(resource, %i[attributes]))
      }
    end

    def find_sender(currency_iso = 'BTC')
      current_user.assets.only_enabled.find_by('balance.currency_iso' => currency_iso)
    end

    def find_receiver(talk, currency_iso = 'BTC')
      talk.talkers.find_by(:user.ne => current_user).user.assets.only_enabled
          .find_by('balance.currency_iso' => currency_iso)
    end

    def new_transaction(sender, receiver, amount, currency_iso = 'BTC')
      Transaction.new(
        category: :transfer, sender: sender, receiver: receiver, amount: Money.from_amount(amount, currency_iso)
      )
    end
  end
end
