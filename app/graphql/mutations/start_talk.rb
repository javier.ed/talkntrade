# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class StartTalk < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    description 'Start a talk for the current user with another'

    argument :id, ID, 'ID of the other user', required: true

    field :talk, Types::TalkType, 'Talk recently started', null: true

    def resolve(id:)
      QueuesService.call(action: :start_talk, sleep: 0.1) do
        resource = find_talk id
        return success_response(resource) if resource

        user = find_user id
        return error_response unless user

        resource = current_user.find_enabled_talk_with(user)
        return success_response(resource) if resource

        resource = Talk.new talkers: [Talker.new(user: current_user), Talker.new(user: user)]

        if resource.save
          success_response(resource)
        else
          error_response
        end
      end
    end

    private

    def find_talk(id)
      current_user.find_enabled_talk(id)
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end

    def find_user(id)
      User.only_enabled.for_user(current_user).find id
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end

    def success_response(resource)
      { success: true, talk: resource }
    end

    def error_response
      { success: false, message: I18n.t('mutations.start_talk.fail') }
    end
  end
end
