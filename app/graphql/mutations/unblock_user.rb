# frozen_string_literal: true

module Mutations
  class UnblockUser < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    description 'Attempt to unblock an user from the current user'

    argument :user_id, ID, 'ID of the user to unblock', required: true

    def resolve(user_id:)
      QueuesService.call(action: :unblock_user, scope: "user_id_#{current_user.id}") do
        resource = current_user.blocked_users.find_by(blocked_user_id: user_id)

        if resource&.destroy
          { success: true, message: I18n.t('mutations.unblock_user.success') }
        else
          error_response
        end
      rescue Mongoid::Errors::DocumentNotFound
        error_response
      end
    end

    private

    def error_response
      { success: false, message: I18n.t('mutations.unblock_user.fail') }
    end
  end
end
