# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class UpdateProfile < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    argument :attributes, Inputs::User::ProfileInput, required: true

    field :profile, Types::User::ProfileType, null: true

    def resolve(attributes:)
      QueuesService.call(action: :update_user, scope: "id_#{current_user.id}") do
        resource = current_user.profile

        assign_avatar_image(resource, attributes)

        if resource.update(
          display_name: attributes.display_name,
          first_name: attributes.first_name,
          last_name: attributes.last_name,
          birthdate: attributes.birthdate,
          country_gec: attributes.country_gec,
          bio: attributes.bio
        )
          { success: true, message: I18n.t('mutations.update_profile.success'), profile: resource }
        else
          {
            success: false,
            message: I18n.t('mutations.update_profile.fail'),
            errors: parse_errors(resource, %w[attributes])
          }
        end
      end
    end

    private

    # Assign a new avatar image or remove the current one
    #
    # @param resource [User::Profile]
    # @param attributes [Inputs::User::ProfileInput]
    def assign_avatar_image(resource, attributes)
      if attributes.avatar_image
        resource.avatar_image = attributes.avatar_image
      elsif attributes.remove_avatar_image
        resource.avatar_image = nil
      end
    end
  end
end
