# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class VerifyEmail < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    argument :token, String, required: true

    def resolve(token:)
      QueuesService.call(action: :update_user, scope: "id_#{current_user.id}") do
        if current_user.verify_email token
          { success: true, message: I18n.t('mutations.verify_email.success') }
        else
          { success: false, message: I18n.t('mutations.verify_email.fail') }
        end
      end
    end
  end
end
