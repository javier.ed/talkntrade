# frozen_string_literal: true

require 'talkntrade/graphql'

module Mutations
  class Withdraw < Mutations::BaseMutation
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    argument :attributes, Inputs::WithdrawInput, required: true

    def resolve(attributes:)
      QueuesService.call(action: :create_transaction, scope: "user_id_#{current_user.id}") do
        sender = current_user.assets.only_enabled.find_by('balance.currency_iso' => attributes.currency_iso_code)

        withdraw = Transaction.withdraw(
          sender: sender, receiver_address: attributes.receiver_address, amount: attributes.amount
        )
        if withdraw.persisted?
          { success: true, message: I18n.t('mutations.withdraw.success') }
        else
          error_response withdraw
        end
      rescue Mongoid::Errors::DocumentNotFound
        error_response
      end
    end

    def error_response(withdraw)
      { success: false, message: I18n.t('mutations.withdraw.fail'), errors: parse_errors(withdraw, %i[attributes]) }
    end
  end
end
