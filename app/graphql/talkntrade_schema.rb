# frozen_string_literal: true

class TalkntradeSchema < GraphQL::Schema
  use GraphQL::Subscriptions::ActionCableSubscriptions
  use GraphQL::Backtrace

  mutation Types::MutationType
  query Types::QueryType
  subscription Types::SubscriptionType

  context_class Contexts::CustomContext

  def self.id_from_object(object, _type, _ctx)
    if object.is_a? Hash
      object[:id].to_s
    else
      object.id.to_s
    end
  end
end
