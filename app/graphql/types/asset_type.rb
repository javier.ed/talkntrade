# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  class AssetType < Types::BaseObject
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    global_id_field :id

    field :balance, Float, null: true
    def balance
      object.balance.to_f
    end

    field :currency, Types::CurrencyType, null: true
    def currency
      object.balance.currency
    end

    field :primary_address, String, null: true

    field :transactions, [Types::TransactionType], null: true
    def transactions
      object.transactions.desc(:id)
    end
  end
end
