# frozen_string_literal: true

module Types
  class BaseObject < GraphQL::Schema::Object
    Mongoid::QueryCache.enabled = true
  end
end
