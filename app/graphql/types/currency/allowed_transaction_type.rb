# frozen_string_literal: true

module Types
  module Currency
    class AllowedTransactionType < Types::BaseObject
      graphql_name 'CurrencyAllowedTransactionType'

      include Talkntrade::Graphql::AuthenticateUser

      global_id_field :id
      field :min_amount, Float, null: true
      field :min_fee, Float, null: true
      field :fee_percentage, Float, null: true
    end
  end
end
