# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  class CurrencyType < Types::BaseObject
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    global_id_field :id
    field :name, String, null: true
    field :iso_code, String, null: true
    field :symbol, String, null: true
    field :subunit, String, null: true
    field :exponent, Int, null: true

    field :enabled, Boolean, 'Return true if the currency is enabled', null: true
    def enabled
      Settings.cryptocurrencies[object.id][:enable]
    end

    field :allowed_transactions, [Types::Currency::AllowedTransactionType], null: true
    def allowed_transactions
      t = []
      Settings.cryptocurrencies[object.id][:allowed_transactions].each do |key, value|
        t << value.merge(id: key)
      end
      t
    end

    field :allowed_transaction, Types::Currency::AllowedTransactionType, null: true do
      argument :id, String, required: true
    end
    def allowed_transaction(id:)
      Settings.cryptocurrencies[object.id][:allowed_transactions][id]
    end
  end
end
