# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  class ImageType < Types::BaseObject
    include Talkntrade::Graphql::RequireDeviceAuthorization
    include Talkntrade::Graphql::Scopes

    scopes :read_product

    field :url, String, null: true
    field :file_name, String, null: true
    field :file_type, String, null: true
    field :size, Int, null: true
    field :width, Int, null: true
    field :height, Int, null: true
    field :orientation, Int, null: true
  end
end
