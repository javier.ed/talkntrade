# frozen_string_literal: true

module Types
  class InfoType < Types::BaseObject
    field :server_version, String, null: true
    def server_version
      Talkntrade::VERSION
    end

    field :server_date, GraphQL::Types::ISO8601DateTime, null: true
    def server_date
      Time.current
    end

    field :client_expires_at, GraphQL::Types::ISO8601DateTime, null: true
    def client_expires_at
      context.current_device&.client_app&.expires_at
    end

    field :client_status, String, null: true
    def client_status
      if context.current_user
        'user_authenticated'
      elsif context.current_device
        'device_authorized'
      else
        'unauthorized'
      end
    end

    field :scopes, [String], null: true
    def scopes
      context.current_device&.client_app&.scopes
    end
  end
end
