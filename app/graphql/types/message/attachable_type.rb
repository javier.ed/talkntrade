# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  module Message
    class AttachableType < Types::BaseUnion
      include Talkntrade::Graphql::AuthenticateUser
      include Talkntrade::Graphql::Scopes

      graphql_name 'MessageAttachableType'

      possible_types Types::TransactionType

      # @param object [Transaction]
      # @param _context [GraphQL::Query::Context]
      #
      # @return [Types::TransactionType]
      def self.resolve_type(object, _context)
        Types::TransactionType if object.is_a? Transaction
      end
    end
  end
end
