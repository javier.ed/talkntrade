# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  class MessageType < Types::BaseObject
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    global_id_field :id

    field :created_at, GraphQL::Types::ISO8601DateTime, null: true
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: true

    field :content, String, null: true
    field :attachable, Types::Message::AttachableType, null: true
    field :talker, Types::TalkerType, null: true

    field :seen, Boolean, null: true
    def seen
      return true if object.talker.user == context.current_user

      talker = object.talk.talkers.find_by user: context.current_user
      recipient = object.recipients.find_by talker: talker

      recipient.seen_at.present?
    rescue Mongoid::Errors::DocumentNotFound
      true
    end

    field :received, Boolean, null: true
    def received
      object.talker.user != context.current_user
    end

    field :talk, Types::TalkType, null: true
  end
end
