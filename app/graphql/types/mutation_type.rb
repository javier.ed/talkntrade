# frozen_string_literal: true

module Types
  class MutationType < Types::BaseObject
    Mongoid::QueryCache.enabled = false

    field :register_device, mutation: Mutations::RegisterDevice

    field :login, mutation: Mutations::Login
    field :logout, mutation: Mutations::Logout
    field :register, mutation: Mutations::Register

    field :update_profile, mutation: Mutations::UpdateProfile

    field :change_email, mutation: Mutations::ChangeEmail
    field :send_email_verification_token, mutation: Mutations::SendEmailVerificationToken
    field :verify_email, mutation: Mutations::VerifyEmail

    field :request_password_reset, mutation: Mutations::RequestPasswordReset
    field :reset_password, mutation: Mutations::ResetPassword
    field :change_password, mutation: Mutations::ChangePassword

    field :mark_seen_message, mutation: Mutations::MarkSeenMessage
    field :send_message, mutation: Mutations::SendMessage
    field :start_talk, mutation: Mutations::StartTalk

    field :withdraw, mutation: Mutations::Withdraw

    field :block_user, mutation: Mutations::BlockUser
    field :unblock_user, mutation: Mutations::UnblockUser

    field :report_user, mutation: Mutations::ReportUser
  end
end
