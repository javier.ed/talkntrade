# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    Mongoid::QueryCache.enabled = true

    field :info, Types::InfoType, null: true
    def info
      {}
    end

    field :currencies, [Types::CurrencyType], 'List all available currencies (enabled or not)', null: true
    def currencies
      Cryptocurrencies.ids.map do |currency_id|
        Money::Currency.find(currency_id)
      end
    end

    field :currency, Types::CurrencyType, 'Find an available currencies', null: true do
      argument :id, ID, 'ID of the currency', required: true
    end
    def currency(id:)
      return nil unless Cryptocurrencies.ids.include?(id)

      Money::Currency.find(id)
    end

    field :current_user, Types::UserType, null: true
    delegate :current_user, to: :context

    field :users, [Types::UserType], 'List enabled users that match the query argument', null: true do
      argument :query, String, required: true do
        description 'Query to match with usernames or display names (must be at least 5 characters)'
      end
    end
    def users(query:)
      return if query.strip.length < 5

      query_regexp = /#{::Regexp.escape(query.strip)}/i

      ::User.only_enabled.for_user(current_user).and(
        '$or' => [{ username: query_regexp }, { 'profile.display_name' => query_regexp }]
      ).asc(:username)
    end

    field :user, Types::UserType, 'Find an enabled user by ID', null: true do
      argument :id, ID, 'ID of the user to find', required: true
    end
    def user(id:)
      ::User.only_enabled.for_user(current_user, exclude_current: false)
            .and('$or' => [{ id: id }, { username: /^#{::Regexp.escape(id)}$/i }]).first
    end

    field :talks, [Types::TalkType], 'List enabled talks of the current user', null: true, extras: [:lookahead]
    def talks(lookahead:)
      only = talks_lookahead(lookahead)
      context.current_user.talks.only(only).only_enabled.exists(last_message_at: true).sort(last_message_at: -1)
    end

    field :talk, Types::TalkType, 'Find an enabled talk of the current user by ID', null: true do
      argument :id, ID, 'ID of the talk to find', required: true
    end
    def talk(id:)
      current_user.find_enabled_talk(id)
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end

    field :message, Types::MessageType, null: true do
      argument :id, ID, required: true
    end
    def message(id:)
      message = ::Message.find id
      return message if message.talk.talkers.find_by(user: current_user)
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end

    field :assets, [Types::AssetType], null: true
    def assets
      context.current_user.assets
    end

    field :asset, Types::AssetType, null: true do
      argument :id, ID, required: true
    end
    def asset(id:)
      context.current_user.assets.find_by(
        '$or' => [{ id: id }, { 'balance.currency_iso' => /^#{::Regexp.escape(id)}$/i }]
      )
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end

    private

    def talks_lookahead(lookahead)
      only = %i[disabled_at]
      lookahead.selections.each do |selection|
        next if %i[__typename id name talkers unseen_count messages current_purchase].include?(selection.name)

        only << selection.name
      end
      only.uniq
    end
  end
end
