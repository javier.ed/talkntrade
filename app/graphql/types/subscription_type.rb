# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  class SubscriptionType < GraphQL::Schema::Object
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    field :new_message, Types::MessageType, null: false, subscription_scope: :current_user_id
    def new_message
      nil
    end
  end
end
