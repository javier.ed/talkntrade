# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  class TalkType < Types::BaseObject
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    global_id_field :id

    field :created_at, GraphQL::Types::ISO8601DateTime, null: true
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: true

    field :name, String, 'Name of the talk (by default, the display name of the other talker)', null: true
    def name
      object.name_for_talker(current_talker)
    end

    field :talkers, [Types::TalkerType], null: true

    field :last_message_at, GraphQL::Types::ISO8601DateTime, null: true

    field :unseen_count, Int, null: true
    def unseen_count
      unseen_messages.count
    end

    field :messages, [Types::MessageType], null: true, extras: [:lookahead] do
      argument :before_id, ID, required: false
      argument :limit, Integer, required: false
      argument :include_unseen, Boolean, required: false
    end
    def messages(before_id: nil, limit: 10, include_unseen: false, lookahead:)
      only = messages_lookahead(lookahead)
      if include_unseen
        first_message_unseen = unseen_messages.first

        return object.messages.only(only).desc(:id).limit(limit).reverse unless first_message_unseen

        offset_messages = object.messages.only(:id).where :id.lt => first_message_unseen.id
        first_message = case offset_messages.count
                        when 0
                          first_unseen
                        when 1..limit
                          offset_messages.last
                        else
                          offset_messages.desc(:id).offset(limit).first
                        end

        object.messages.only(only).where(:id.gte => first_message.id)
      elsif before_id
        object.messages.only(only).where(:id.lt => before_id).desc(:id).limit(limit).reverse
      else
        object.messages.only(only).desc(:id).limit(limit).reverse
      end
    end

    private

    def messages_lookahead(lookahead)
      only = []
      lookahead.selections.each do |selection|
        next if %i[__typename id].include?(selection.name)

        only << case selection.name
                when :talker, :received
                  :talker_id
                when :seen
                  only += %i[talker_id recipients]
                  :talk_id
                when :talk
                  :talk_id
                when :attachable
                  only << :attachable_type
                  :attachable_id
                else
                  selection.name
                end
      end
      only.uniq
    end

    # Return the current talker
    #
    # @return [Talker, nil]
    def current_talker
      object.talkers.only(:user_id).find_by(user: context.current_user)
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end

    # Return unseen messages for the current talker
    def unseen_messages
      object.messages.only(:id).where(:recipients.elem_match => { talker_id: current_talker&.id, seen_at: nil })
    end
  end
end
