# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  class TalkerType < Types::BaseObject
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    global_id_field :id
    field :user, Types::UserType, null: true
  end
end
