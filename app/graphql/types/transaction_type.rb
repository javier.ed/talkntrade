# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  class TransactionType < Types::BaseObject
    include Talkntrade::Graphql::AuthenticateUser
    include Talkntrade::Graphql::Scopes

    global_id_field :id
    field :created_at, GraphQL::Types::ISO8601DateTime, null: true
    field :number, Int, null: true
    field :category, String, null: true
    field :txid, String, null: true
    field :amount, Float, null: true
    field :fee, Float, null: true
    field :currency, Types::CurrencyType, null: true
    field :receiver_address, String, null: true

    field :received, Boolean, null: true
    def received
      object&.receiver&.user == context.current_user
    end

    field :tx_url, String, null: true
    def tx_url
      return nil if object.txid.blank?

      URI.join(Settings.cryptocurrencies[:btc][:tx_base_url], object.txid)
    end
  end
end
