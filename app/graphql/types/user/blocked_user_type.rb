# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  module User
    class BlockedUserType < Types::BaseObject
      include Talkntrade::Graphql::AuthenticateUser
      include Talkntrade::Graphql::Scopes

      graphql_name 'UserBlockedUserType'

      global_id_field :id

      field :created_at, GraphQL::Types::ISO8601DateTime, null: true
      field :updated_at, GraphQL::Types::ISO8601DateTime, null: true
      field :blocked_user, Types::UserType, null: true
    end
  end
end
