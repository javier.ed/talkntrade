# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  module User
    class ProfileType < Types::BaseObject
      include Talkntrade::Graphql::RequireDeviceAuthorization
      include Talkntrade::Graphql::Scopes

      graphql_name 'UserProfileType'

      scopes :read_product

      global_id_field :id
      field :display_name, String, null: true
      field :first_name, String, null: true
      field :last_name, String, null: true
      field :birthdate, GraphQL::Types::ISO8601DateTime, null: true
      field :country_gec, String, null: true
      field :bio, String, null: true
    end
  end
end
