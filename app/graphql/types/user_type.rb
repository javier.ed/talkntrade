# frozen_string_literal: true

require 'talkntrade/graphql'

module Types
  class UserType < Types::BaseObject
    include Talkntrade::Graphql::RequireDeviceAuthorization
    include Talkntrade::Graphql::Scopes

    scopes :read_product

    global_id_field :id
    field :created_at, GraphQL::Types::ISO8601DateTime, null: true
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: true
    field :username, String, null: true

    field :profile, Types::User::ProfileType, null: true

    field :email, String, null: true do
      def authorized?(object, args, context)
        super && object.id == context.current_user&.id
      end
    end

    field :email_verified, Boolean, null: true do
      def authorized?(object, args, context)
        super && object.id == context.current_user&.id
      end
    end
    def email_verified
      object.email_verified_at.present?
    end

    field :talks, [Types::TalkType], 'Talks of the current user', null: true do
      def authorized?(object, args, context)
        super && object.id == context.current_user&.id
      end
    end
    def talks
      object.talks.only_enabled.exists(last_message_at: true).sort(last_message_at: -1)
    end

    field :blocked_users, [Types::User::BlockedUserType], null: true do
      description 'Blocked users (only available for the current user)'

      def authorized?(object, args, context)
        super && object.id == context.current_user&.id
      end
    end

    field :webclient_url, String, 'Webclient URL', null: true
    def webclient_url
      URI.join(Settings.webclient_url, 'users', object.username) if Settings.webclient_url
    end
  end
end
