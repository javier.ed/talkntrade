# frozen_string_literal: true

class TransactionsJob < ApplicationJob
  include Sidekiq::Worker::Options

  sidekiq_options retry: 0
  queue_as :default

  def perform(*_args)
    QueuesService.call(action: :register_deposits) do
      register_deposits Cryptocurrencies::Bitcoin
      register_deposits Cryptocurrencies::Litecoin
    end
  rescue StandardError => e
    Rails.logger.error [e.message, *e.backtrace].join($RS)
  end

  def register_deposits(cryptocurrency_class)
    return unless cryptocurrency_class.settings[:enable]

    limit = cryptocurrency_class.getwalletinfo['txcount']

    cryptocurrency_class.listtransactions('*', limit).each do |transaction|
      next unless transaction['category'] == 'receive' && transaction['confirmations'] >= 5

      Transaction.register_deposit(
        txid: transaction['txid'], receiver_address: transaction['address'], total_amount: transaction['amount'].abs,
        currency_iso: 'BTC'
      )
    end
  end
end
