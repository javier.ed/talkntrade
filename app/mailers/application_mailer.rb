# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: Settings.name + ' <' + Settings.mail[:sender_address] + '>'
  layout 'mailer'
end
