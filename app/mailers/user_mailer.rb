# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def email_verification_mail
    @username = params[:username]
    @email_verification_token = params[:email_verification_token]

    mail to: params[:to]
  end
end
