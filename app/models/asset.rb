# frozen_string_literal: true

require 'mongoid/disabler'

class Asset
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Disabler
  include Mongoid::History::Trackable

  track_history modifier_field_optional: true, on: %i[disabled_at]

  attr_readonly :created_at, :user_id

  field :balance, type: Money, default: 0

  belongs_to :user
  embeds_many :addresses, class_name: 'Asset::Address', cascade_callbacks: true
  has_many :sent_transactions, class_name: 'Transaction', inverse_of: :sender, dependent: :restrict_with_exception
  has_many :received_transactions, class_name: 'Transaction', inverse_of: :receiver, dependent: :restrict_with_exception

  validate :validate_uniqueness_of_currency

  after_create :create_primary_address
  after_find :create_primary_address

  def transactions
    Transaction.or [{ sender: self }, { receiver: self }]
  end

  def primary_address
    addresses.find_by(label: :primary).address
  rescue Mongoid::Errors::DocumentNotFound
    nil
  end

  delegate :currency, to: :balance

  private

  def create_primary_address
    return if addresses.find_by(label: :primary)

    addresses.create
  rescue Mongoid::Errors::DocumentNotFound
    addresses.create
  end

  def validate_uniqueness_of_currency
    Asset.find_by(:id.ne => id, 'balance.currency_iso' => currency.iso_code, user: user)
    errors.add(:currency)
  rescue Mongoid::Errors::DocumentNotFound
    nil
  end
end
