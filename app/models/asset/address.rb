# frozen_string_literal: true

class Asset
  class Address
    include Mongoid::Document
    include Mongoid::Timestamps

    attr_readonly :created_at, :address, :label

    field :address, type: String
    field :label, type: Symbol, default: :primary

    embedded_in :asset, class_name: 'Asset'

    validates :address, presence: true
    validates :label, inclusion: { in: %i[primary backup] }
    validate :only_primary

    before_validation :assign_address

    private

    def only_primary
      return true unless label == :primary && asset&.addresses&.find_by(:id.ne => id, label: :primary)

      errors.add(:label)
      false
    rescue Mongoid::Errors::DocumentNotFound
      true
    end

    def assign_address
      return if address.present?

      self.address = case asset.currency.id
                     when :btc
                       Cryptocurrencies::Bitcoin.getnewaddress(label)
                     when :ltc
                       Cryptocurrencies::Litecoin.getnewaddress(label)
                     end
    rescue StandardError => e
      Rails.logger.error e.backtrace.join($RS)
    end
  end
end
