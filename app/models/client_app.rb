# frozen_string_literal: true

require 'mongoid/secret_key'

class ClientApp
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::History::Trackable
  include Mongoid::SecretKey

  track_history modifier_field_optional: true, on: %i[expires_at description scopes]

  attr_readonly :created_at, :secret_key

  field :expires_at, type: Time
  field :description, type: String
  field :scopes, type: Array

  has_secret_key

  has_many :devices, dependent: :restrict_with_exception

  validates :scopes, inclusion: { in: [%i[all], %i[read_product]] }

  def unexpired?
    expires_at.blank? || expires_at > Time.current
  end

  def self.find_unexpired(id)
    find_by id: id, '$or' => [{ expires_at: nil }, { :expires_at.gt => Time.current }]
  rescue Mongoid::Errors::DocumentNotFound
    nil
  end

  # Decodes a JWT by using the secret key
  #
  # @param jwt [String] JWT encoded with the secret key
  # @return [Array, nil] JWT decoded or nil
  def jwt_decode(jwt)
    JWT.decode(jwt, secret_key, true, verify_iat: true)
  rescue JWT::DecodeError
    nil
  end

  # Encodes a payload into a JWT by using the secret key
  #
  # @param payload [Object] Payload
  # @return [String, nil] JWT encoded or nil
  def jwt_encode(payload)
    JWT.encode(payload, secret_key)
  rescue JWT::EncodeError
    nil
  end
end
