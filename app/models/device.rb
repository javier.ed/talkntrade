# frozen_string_literal: true

require 'mongoid/secret_key'

class Device
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::History::Trackable
  include Mongoid::SecretKey

  track_history modifier_field_optional: true, on: %i[client_app_id embedded_documents]

  attr_readonly :created_at, :secret_key

  has_secret_key

  belongs_to :client_app
  embeds_many :sessions, class_name: 'Device::Session'
  embeds_many :login_attempts, class_name: 'Device::LoginAttempt'

  field :blocked_at, type: Time

  # Add criteria to return only unblocked devices
  scope :only_unblocked, -> { where(blocked_at: nil) }
end
