# frozen_string_literal: true

class Device
  # Stores a counter of failed login attempts in a device for every user
  class LoginAttempt
    include Mongoid::Document
    include Mongoid::Timestamps

    attr_readonly :created_at, :user_id

    embedded_in :device
    belongs_to :user

    field :counter, type: Integer, default: 0

    validates :counter, numericality: { greater_than_or_equal_to: 0 }

    after_save :block_device_or_not

    # Increment the counter of failed login attempts
    #
    # @return [Boolean]
    def increment_counter
      update(counter: (counter + 1))
    end

    # Reset the counter of failed login attempts
    #
    # @return [Boolean]
    def reset_counter
      update(counter: 0)
    end

    private

    # Block the device if the counter is equal or greater to the maximum
    def block_device_or_not
      return if counter < Settings.max_login_attempts

      device.update(blocked_at: Time.current)
    end
  end
end
