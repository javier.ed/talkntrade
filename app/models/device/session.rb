# frozen_string_literal: true

class Device
  class Session
    include Mongoid::Document
    include Mongoid::Timestamps

    attr_readonly :created_at, :user_id

    field :last_request_at, type: Time
    field :finished_at, type: Time

    belongs_to :user, class_name: 'User'
    embedded_in :device, class_name: 'Device'
  end
end
