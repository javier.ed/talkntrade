# frozen_string_literal: true

class Message
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  attr_readonly :created_at, :content, :talk_id, :talker_id, :attachable_id, :attachable_type

  field :content, type: String

  belongs_to :talk
  belongs_to :talker

  belongs_to :attachable, polymorphic: true, optional: true
  embeds_many :recipients, class_name: 'Message::Recipient'

  validates :content, presence: true
  validates :attachable_type, inclusion: { in: %w[Transaction] }, allow_blank: true

  validates_associated :attachable, if: proc { attachable_type == 'Transaction' }
  accepts_nested_attributes_for :attachable, if: proc { attachable_type == 'Transaction' }

  after_create :add_recipients
  after_create :update_talk

  def self.send_message(talk:, talker:, content:, attachable: nil)
    message = new talk: talk, talker: talker, content: content, attachable: attachable

    return false unless message.save

    talk.talkers.each do |t|
      TalkntradeSchema.subscriptions.trigger('new_message', {}, message, scope: t.user.id)
    end

    true
  rescue Mongoid::Errors::DocumentNotFound
    false
  end

  private

  # Add message recipients
  def add_recipients
    talk.talkers.ne(id: talker.id).each do |t|
      recipients.create(talker: t)
    end
  end

  def update_talk
    talk.last_message_at = Time.current
    talk.save touch: false
  end
end
