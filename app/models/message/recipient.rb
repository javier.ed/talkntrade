# frozen_string_literal: true

class Message
  # Store the recipients of a message
  class Recipient
    include Mongoid::Document
    include Mongoid::Timestamps

    attr_readonly :created_at, :talker_id

    embedded_in :message
    belongs_to :talker

    field :received_at, type: Time
    field :seen_at, type: Time

    after_save :mark_previous

    # Add criteria to return only unseen recipients
    scope :only_unseen, -> { where(seen_at: nil) }

    private

    # Mark the previous messages as seen
    def mark_previous
      return if seen_at.blank?

      message.talk.messages
             .elem_match(recipients: { talker_id: talker_id, seen_at: nil, :created_at.lte => created_at }).each do |m|
        m.recipients.where(talker: talker, seen_at: nil, :created_at.lte => created_at).each do |r|
          r.update(seen_at: Time.current)
        end
      end
    end
  end
end
