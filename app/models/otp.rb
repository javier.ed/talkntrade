# frozen_string_literal: true

require 'mongoid/disabler'

class Otp
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword
  include Mongoid::Disabler
  include Mongoid::History::Trackable

  track_history modifier_field_optional: true, on: %i[disabled_at]

  attr_readonly :created_at, :action, :password_digest

  has_secure_password

  field :action, type: Symbol
  field :password_digest, type: String
  field :used_at, type: Time
  field :discarded_at, type: Time
  field :attempts, type: Integer, default: 0

  belongs_to :user

  validates :action, inclusion: { in: %i[reset_password] }
  validates :attempts, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 3 }

  def self.find_and_authenticate(id, action, password)
    otp = find_by id: id, action: action, used_at: nil, discarded_at: nil, :attempts.lt => 3
    return false unless otp

    if otp.authenticate(password)
      otp
    else
      otp.inc attempts: 1
      false
    end
  rescue Mongoid::Errors::DocumentNotFound
    false
  end

  def mark_as_used
    update used_at: Time.current
  end

  def self.only_unused
    where(used_at: nil, disabled_at: nil)
  end

  after_initialize :generate_password, if: proc { password_digest.blank? }
  after_create :discard_others
  after_create :send_password

  private

  def generate_password
    self.password = SecureRandom.base58(6).upcase
  end

  def discard_others
    user.otps.where(:id.ne => id, action: action, used_at: nil, discarded_at: nil, :attempts.lte => 3)
        .update_all discarded_at: Time.current # rubocop:disable Rails/SkipsModelValidations
  end

  def send_password
    OtpMailer.with(to: user.email, username: user.username, password: password, action: action.to_s).password_mail
             .deliver_later
  end
end
