# frozen_string_literal: true

class ReportedUser
  include Mongoid::Document
  include Mongoid::Timestamps

  attr_readonly :created_at, :user_id, :reported_user_id

  belongs_to :user, inverse_of: :reported_users
  belongs_to :reported_user, class_name: 'User', inverse_of: :reports_received

  field :category, type: Symbol
  field :subject, type: String
  field :message, type: String

  validates :category, inclusion: { in: Settings.reported_user_categories }
  validates :subject, :message, presence: true
  validates :subject, length: { in: 5..256 }, allow_blank: true
  validates :subject, single_line_string: true, allow_blank: true
  validates :reported_user_id, uniqueness: { scope: %i[user_id category] }
  validate :validate_reported_user

  private

  def validate_reported_user
    errors.add(:reported_user) if user == reported_user
  end
end
