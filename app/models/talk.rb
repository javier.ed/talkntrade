# frozen_string_literal: true

require 'mongoid/disabler'

class Talk
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::History::Trackable
  include Mongoid::Disabler

  track_history modifier_field_optional: true, on: %i[disabled_at]

  field :last_message_at, type: Time

  has_many :messages, dependent: :restrict_with_exception
  has_many :talkers, dependent: :restrict_with_exception, autosave: true

  validates :talkers, length: { is: 2 }, on: :create
  validate :validate_talkers, if: proc { talkers.size == 2 }, on: :create

  validates_associated :talkers
  accepts_nested_attributes_for :talkers

  # Show the name of the talk for a certain talker
  #
  # @param talker [Talker]
  # @return [String, nil]
  def name_for_talker(talker)
    return nil unless talkers.include?(talker)

    talkers.find_by(:id.ne => talker.id)&.user&.profile&.display_name
  rescue Mongoid::Errors::DocumentNotFound
    nil
  end

  private

  def validate_talkers
    return if talkers.first.user != talkers.last.user && talkers.first.user.find_talk_with(talkers.last.user).nil?

    errors.add(:talkers)
  end
end
