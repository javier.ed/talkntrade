# frozen_string_literal: true

require 'mongoid/disabler'

class Talker
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::History::Trackable
  include Mongoid::Disabler

  track_history modifier_field_optional: true, on: %i[disabled_at]

  attr_readonly :created_at, :user_id, :talk_id

  belongs_to :user
  belongs_to :talk

  has_many :messages, dependent: :restrict_with_exception

  validates :user_id, uniqueness: { scope: :talk_id }

  index({ user_id: 1, talk_id: 1 }, unique: true)

  def disable
    return unless enabled? && update(disabled_at: Time.current)

    talk.disable

    true
  end

  def enable
    return unless disabled? && update(disabled_at: nil)

    talk.enable

    true
  end
end
