# frozen_string_literal: true

require 'autoinc'

class Transaction
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Autoinc

  attr_readonly :created_at, :number, :category, :amount, :fee, :txid, :receiver_address_id, :receiver_address,
                :sender_id, :receiver_id

  attr_accessor :total_amount

  field :number, type: Integer
  field :category, type: Symbol
  field :amount, type: Money, default: 0
  field :fee, type: Money, default: 0
  field :txid, type: String
  field :receiver_address_id, type: BSON::ObjectId
  field :receiver_address, type: String

  belongs_to :sender, class_name: 'Asset', inverse_of: :sent_transactions, optional: true
  belongs_to :receiver, class_name: 'Asset', inverse_of: :received_transactions, optional: true

  validates :category, inclusion: { in: %i[deposit withdrawal transfer] }
  validates :txid, uniqueness: { case_sensitive: false }, allow_blank: true
  validate :validate_min_amount, if: proc { category.present? }, on: :create
  validates :sender, presence: true, if: proc { |a| %i[withdrawal transfer].include?(a.category) }
  validates :receiver, presence: true, if: proc { |a| %i[deposit transfer].include?(a.category) }
  validates :receiver_address, presence: true
  validates :receiver_address, length: { in: 26..35 }, allow_blank: true
  validate :validate_available_balance, if: proc { |a| %i[withdrawal transfer].include?(a.category) }, on: :create
  validate :validate_receiver_address_for_withdrawal, on: :create, if: proc { category == :withdrawal }
  validate :validate_receiver_currency, on: :create, if: proc { %i[deposit transfer].include?(category) }
  validate :validate_sender_availability, on: :create, if: proc { %i[withdrawal transfer].include?(category) }
  validate :validate_receiver_availability, on: :create, if: proc { %i[deposit transfer].include?(category) }

  increments :number

  before_validation :calculate_amounts, on: :create, if: proc { category.present? }
  before_create :run_withdrawal
  after_create :update_balances

  def receiver_address
    return receiver.addresses.find(receiver_address_id).address if receiver_address_id

    return self[:receiver_address] if category == :withdrawal

    receiver&.primary_address
  rescue Mongoid::Errors::DocumentNotFound
    nil
  end

  delegate :currency, to: :amount

  def self.register_deposit(txid:, receiver_address:, total_amount:, currency_iso: 'BTC')
    receiver = Asset.find_by('balance.currency_iso' => currency_iso,
                             'addresses.address' => /^#{::Regexp.escape(receiver_address)}$/i)

    create(
      category: :deposit, txid: txid, receiver: receiver, amount: Money.new(0, currency_iso),
      fee: Money.new(0, currency_iso), total_amount: Money.from_amount(total_amount, currency_iso)
    )
  rescue Mongoid::Errors::DocumentNotFound
    false
  end

  def self.withdraw(sender:, receiver_address:, amount:)
    create(
      category: :withdrawal, sender: sender, receiver_address: receiver_address,
      amount: Money.from_amount(amount, sender.currency.id)
    )
  end

  def self.transfer(sender:, receiver:, amount:)
    create(
      category: :transfer, sender: sender, receiver: receiver, amount: Money.from_amount(amount, sender.currency.id)
    )
  end

  private

  def allowed_transactions
    Settings.cryptocurrencies[currency.id][:allowed_transactions]
  end

  def min_amount
    Money.from_amount(allowed_transactions[category][:min_amount], currency.id)
  end

  def min_fee
    Money.from_amount(allowed_transactions[category][:min_fee], currency.id)
  end

  def fee_percentage
    allowed_transactions[category][:fee_percentage]
  end

  def fee_from_amount(value)
    if ((value * fee_percentage) / (1 - fee_percentage)) > min_fee
      ((value * fee_percentage) / (1 - fee_percentage))
    else
      min_fee
    end
  end

  def fee_from_total_amount(value)
    return (value * fee_percentage) if (value * fee_percentage) > min_fee

    min_fee
  end

  def calculate_amounts
    if total_amount.present? && total_amount.positive?
      self.fee = fee_from_total_amount(total_amount)
      self.amount = total_amount - fee
    elsif amount.positive?
      self.fee = fee_from_amount(amount)
      self.total_amount = amount + fee
    end
  end

  def validate_min_amount
    if total_amount < (min_amount + min_fee)
      errors.add(:total_amount, :greater_than_or_equal_to, count: (min_amount + min_fee))
    end
    errors.add(:amount, :greater_than_or_equal_to, count: min_amount) if amount < min_amount
  end

  def cryptocurrency_class
    case currency.id
    when :btc
      Cryptocurrencies::Bitcoin
    when :ltc
      Cryptocurrencies::Litecoin
    end
  end

  def validate_available_balance
    available_balance = Money.from_amount(cryptocurrency_class.getwalletinfo['balance'], currency.id)

    return unless sender && (sender.balance < total_amount || available_balance < total_amount)

    errors.add(:total_amount, :insufficient_funds)

    errors.add(:amount, :insufficient_funds)
  end

  def validate_receiver_address_for_withdrawal
    return unless cryptocurrency_class.getaddressinfo(receiver_address)['ismine']

    errors.add(:receiver_address)
  rescue Cryptocurrencies::Base::RPCError
    errors.add(:receiver_address)
  rescue StandardError => e
    errors.add(:receiver_address)
    Rails.logger.error [e.message, *e.backtrace].join($RS)
  end

  def validate_receiver_currency
    errors.add(:receiver) if receiver.currency != currency
  end

  def validate_sender_availability
    errors.add(:sender) if sender.disabled? || !cryptocurrency_class.settings[:enable]
  end

  def validate_receiver_availability
    errors.add(:receiver) if receiver.disabled? || !cryptocurrency_class.settings[:enable]
  end

  def run_withdrawal
    return unless category == :withdrawal

    self.txid = cryptocurrency_class.sendtoaddress receiver_address, amount.to_f
  rescue StandardError => e
    Rails.logger.error e.backtrace.join('\n')
  end

  def update_balances
    sender&.inc('balance.cents' => - total_amount.cents)
    receiver&.inc('balance.cents' => amount.cents)
  end
end
