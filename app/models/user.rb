# frozen_string_literal: true

require 'mongoid/disabler'

class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::History::Trackable
  include ActiveModel::SecurePassword
  include Mongoid::Disabler

  track_history modifier_field_optional: true,
                on: %i[email email_verification_token email_verified_at password_digest disabled_at]

  attr_readonly :created_at, :username

  has_secure_password

  field :username, type: String
  field :email, type: String
  field :email_verification_token, type: String
  field :email_verified_at, type: Time
  field :password_digest, type: String

  embeds_many :blocked_users, class_name: 'User::BlockedUser'
  embeds_one :profile, class_name: 'User::Profile', autobuild: true, cascade_callbacks: true
  has_many :assets, dependent: :restrict_with_exception
  has_many :talkers, dependent: :restrict_with_exception
  has_many :otps, dependent: :restrict_with_exception
  has_many :reported_users, inverse_of: :user, dependent: :restrict_with_exception
  has_many :reports_received, class_name: 'ReportedUser', inverse_of: :reported_user,
                              dependent: :restrict_with_exception

  validates :username, presence: true
  validates :username, length: { minimum: 5, maximum: 20 }, allow_blank: true
  validates :username, format: { with: /\A[a-z0-9_\-.]+\z/i }, allow_blank: true
  validates :username, exclusion: { in: Settings.username_blacklist }
  validates :username, uniqueness: { case_sensitive: false }
  validates :email, presence: true
  validates :email, length: { minimum: 5, maximum: 256 }, allow_blank: true
  validates :email, format: { with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i }, allow_blank: true
  validates :email, uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 8 }, allow_blank: true

  def talks
    Talk.in(id: talkers.collect(&:talk_id))
  end

  def find_enabled_talk(talk_id)
    talk_id = BSON::ObjectId(talk_id) unless talk_id.is_a?(BSON::ObjectId)

    talk_ids = talkers.only_enabled.map(&:talk_id)

    return nil unless talk_ids.include?(talk_id)

    Talk.only_enabled.find(talk_id)
  rescue Mongoid::Errors::DocumentNotFound, BSON::ObjectId::Invalid
    nil
  end

  def find_enabled_talk_with(user)
    find_talk_with(user, disabled: false)
  end

  def find_talk_with(user, disabled: true)
    talk_id = if disabled
                talks.map(&:id) & user.talks.map(&:id)
              else
                talks.only_enabled.map(&:id) & user.talks.only_enabled.map(&:id)
              end

    return nil if talk_id.empty?

    talks = disabled ? Talk : Talk.only_enabled

    talks.find(talk_id[0])
  rescue Mongoid::Errors::DocumentNotFound
    nil
  end

  def change_password(new_password, new_password_confirmation)
    if new_password.blank?
      errors.add :password, :blank
      return false
    end

    update password: new_password, password_confirmation: new_password_confirmation
  end

  def send_email_verification_token
    return false unless email_verified_at.nil?

    UserMailer.with(to: email, username: username, email_verification_token: email_verification_token)
              .email_verification_mail.deliver_later

    true
  end

  def change_email(new_email)
    self.email = new_email

    unless email_changed?
      errors.add :email
      return false
    end

    save && send_email_verification_token
  end

  def verify_email(token)
    return false unless email_verification_token == token && email_verified_at.nil?

    update email_verified_at: Time.current
  end

  def request_password_reset
    otps.create! action: :reset_password
  rescue Mongoid::Errors::Validations
    nil
  end

  def self.for_user(user, exclude_current: true)
    where(
      '$and' => [
        { :id.ne => exclude_current ? user&.id : nil },
        { :id.nin => (user&.blocked_users&.collect(&:blocked_user_id) || []) }
      ],
      '$or' => [
        { blocked_users: nil },
        { blocked_users: [] },
        { :blocked_users.elem_match => { :blocked_user_id.ne => user&.id } }
      ]
    )
  end

  def self.find_by_login(value)
    find_by(
      '$or' => [
        { username: /^#{::Regexp.escape(value)}$/i },
        { email: /^#{::Regexp.escape(value)}$/i, :email_verified_at.ne => nil }
      ]
    )
  end

  def active_sessions
    sessions = []

    Device.where(:sessions.elem_match => { user_id: id, finished_at: nil }).each do |device|
      sessions += device.sessions.where(user: self, finished_at: nil).to_a
    end

    sessions
  end

  def disable
    return unless enabled? && update(disabled_at: Time.current)

    active_sessions.each { |session| session.update(finished_at: Time.current) }
    assets.each(&:disable)
    talkers.each(&:disable)
    otps.only_unused.each(&:disable)

    true
  end

  def enable
    return unless disabled? && update(disabled_at: nil)

    assets.each(&:enable)
    talkers.each(&:enable)
    otps.only_unused.each(&:enable)

    true
  end

  after_create :create_assets
  after_find :create_assets
  before_save :regenerate_email_verification_token, if: proc { email_changed? }
  after_create :send_email_verification_token

  index({ username: 1 }, unique: true)
  index({ email: 1 }, unique: true)
  index('profile.display_name' => 1)

  private

  def create_assets
    Settings.cryptocurrencies.each do |currency_id, currency_config|
      balance = Money.new(0, currency_id)
      if currency_config[:enable] && assets.where('balance.currency_iso' => balance.currency.iso_code).count.zero?
        assets.create(balance: balance)
      end
    rescue Money::Currency::UnknownCurrency => e
      Rails.logger.error [e.message, *e.backtrace].join($RS)
    end
  end

  def regenerate_email_verification_token
    self.email_verification_token = SecureRandom.base58(6).upcase
    self.email_verified_at = nil
  end
end
