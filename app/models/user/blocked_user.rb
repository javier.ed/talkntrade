# frozen_string_literal: true

class User
  class BlockedUser
    include Mongoid::Document
    include Mongoid::Timestamps

    attr_readonly :created_at, :blocked_user_id

    embedded_in :user

    belongs_to :blocked_user, class_name: 'User'

    validate :validate_user_to_block

    after_create :disable_talk
    after_destroy :enable_talk

    private

    def validate_user_to_block
      errors.add(:blocked_user) if user.blocked_users.find_by(:id.ne => id, blocked_user: blocked_user)
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end

    def disable_talk
      blocked_user.find_enabled_talk_with(user)&.disable
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end

    def enable_talk
      blocked_user.find_talk_with(user)&.enable
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end
  end
end
