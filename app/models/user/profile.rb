# frozen_string_literal: true

class User
  class Profile
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::History::Trackable

    track_history modifier_field_optional: true, scope: :user,
                  on: %i[display_name first_name last_name birthdate country bio]

    attr_readonly :created_at

    embedded_in :user

    mount_uploader :avatar_image, ImageUploader

    field :display_name, type: String
    field :first_name, type: String
    field :last_name, type: String
    field :birthdate, type: Time
    field :country, type: ISO3166::Country
    field :bio, type: String

    validates :display_name, length: { maximum: 128 }, allow_blank: true
    validates :first_name, :last_name, length: { maximum: 256 }, allow_blank: true
    validates :display_name, :first_name, :last_name, single_line_string: true, allow_blank: true
    validates :birthdate, timeliness: { on_or_before: -> { Date.current }, type: :date }, allow_blank: true

    def display_name
      self[:display_name].presence || user.username
    end

    def country_gec
      country&.gec
    end

    def country_gec=(country_gec)
      self.country = country_gec
    end
  end
end
