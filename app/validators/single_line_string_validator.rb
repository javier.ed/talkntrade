# frozen_string_literal: true

class SingleLineStringValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors.add(attribute, :invalid) unless /\A[\wáéíóúàèìòùäëïöüñ\ \.:,;\-()\[\]#%&"'´`]*\z/i.match?(value)
  end
end
