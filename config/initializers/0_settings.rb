# frozen_string_literal: true

module Settings
  def self.recursive_merge(old_hash, new_hash)
    old_hash.merge(new_hash) do |_key, old_var, new_var|
      if old_var.is_a? Hash
        recursive_merge old_var, new_var.to_h
      else
        new_var
      end
    end
  end

  private_class_method :recursive_merge

  SOURCE = recursive_merge(
    {
      'default' => {
        'host' => 'localhost',
        'port' => 3000,
        'protocol' => 'http',

        'name' => "Talk'nTrade",
        'description' => 'Instant messaging service with cryptocurrency transactions',
        'contact_mail' => nil,

        'webclient_url' => nil,

        'locales' => %w[en es],
        'username_blacklist' => %w[talkntrade],
        'max_login_attempts' => 10,
        'mail' => {
          'enable' => false,
          'sender_address' => 'no-reply@localhost',
          'method' => 'sendmail',
          'location' => '/usr/sbin/sendmail',
          'exim_fix' => false,
          'host' => 'localhost',
          'post' => 587,
          'authentication' => 'plain',
          'username' => nil,
          'password' => nil,
          'starttls_auto' => true,
          'domain' => nil,
          'openssl_verify_mode' => nil,
          'ca_file' => nil
        },
        'cryptocurrencies' => {
          'btc' => {
            'enable' => true,
            'rpc_url' => 'http://localhost:18332',
            'tx_base_url' => 'https://live.blockcypher.com/btc-testnet/tx/',
            'allowed_transactions' => {
              'deposit' => {
                'min_amount' => 0.00000001,
                'min_fee' => 0.000001,
                'fee_percentage' => 0.0001
              },
              'transfer' => {
                'min_amount' => 0.00000001,
                'min_fee' => 0.000001,
                'fee_percentage' => 0.0001
              },
              'withdrawal' => {
                'min_amount' => 0.00000001,
                'min_fee' => 0.00001,
                'fee_percentage' => 0.001
              }
            }
          },
          'ltc' => {
            'enable' => false,
            'rpc_url' => 'http://localhost:19332',
            'tx_base_url' => 'https://blockexplorer.one/ltc/testnet/tx/',
            'allowed_transactions' => {
              'deposit' => {
                'min_amount' => 0.00000001,
                'min_fee' => 0.000001,
                'fee_percentage' => 0.0001
              },
              'transfer' => {
                'min_amount' => 0.00000001,
                'min_fee' => 0.000001,
                'fee_percentage' => 0.0001
              },
              'withdrawal' => {
                'min_amount' => 0.00000001,
                'min_fee' => 0.00001,
                'fee_percentage' => 0.001
              }
            }
          }
        },
        'queues' => {
          'redis_url' => 'redis://127.0.0.1:6379/2'
        },
        'reported_user_categories' => %i[
          copyright_infringement spamming malware_distribution other
        ],
        'fail2ban' => {
          'enable' => false,
          'logfile' => 'log/fail2ban.log'
        }
      }, Rails.env => {}
    }, YAML.load_file(Rails.root.join('config', 'settings.yml')) || {}
  )

  SETTINGS = recursive_merge(SOURCE['default'], SOURCE[Rails.env]).with_indifferent_access

  private_constant :SOURCE, :SETTINGS

  def self.host
    SETTINGS[:host]
  end

  def self.port
    SETTINGS[:port].to_i
  end

  def self.protocol
    SETTINGS[:protocol]
  end

  def self.name
    SETTINGS[:name]
  end

  def self.description
    SETTINGS[:description]
  end

  # To get the webclient URL
  #
  # @return [String]
  def self.webclient_url
    SETTINGS[:webclient_url]
  end

  def self.locales
    SETTINGS[:locales]
  end

  def self.contact_mail
    SETTINGS[:contact_mail]
  end

  def self.mail
    SETTINGS[:mail]
  end

  def self.cryptocurrencies
    SETTINGS[:cryptocurrencies]
  end

  def self.username_blacklist
    SETTINGS[:username_blacklist]
  end

  # To get the maximum amount of login attempts before a device should be blocked
  #
  # @return [Integer]
  def self.max_login_attempts
    SETTINGS[:max_login_attempts]
  end

  def self.url
    "#{protocol}://#{host}#{":#{port}" unless [80, 443].include? port}#{start_url}"
  end

  def self.queues
    SETTINGS[:queues]
  end

  def self.reported_user_categories
    SETTINGS[:reported_user_categories].map(&:to_sym)
  end

  # To get Fail2ban settings
  #
  # @return [Hash]
  def self.fail2ban
    SETTINGS[:fail2ban]
  end
end
