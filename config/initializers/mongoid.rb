# frozen_string_literal: true

Mongoid::QueryCache.enabled = false

Mongoid::History.tracker_class_name = :history_tracker
