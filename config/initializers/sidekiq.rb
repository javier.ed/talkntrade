# frozen_string_literal: true

if Rails.env.production?
  Rails.application.configure do
    config.active_job.queue_adapter = :sidekiq
  end

  Sidekiq::Cron::Job.create(name: 'TransactionsJob', cron: '* * * * *', class: 'TransactionsJob', active_job: true)
end
