# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post '/graphql', to: 'graphql#execute'

  scope '/uploads' do
    get '/users/:id/avatar_image', to: 'uploads#user_avatar_image', as: :uploads_users_avatar_image
  end

  root to: 'home#index'
end
