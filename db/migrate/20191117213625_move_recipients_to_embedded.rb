# frozen_string_literal: true

class MoveRecipientsToEmbedded < Mongoid::Migration
  def self.up
    db = Mongoid.default_client.database

    db.collection('recipients').find.each do |recipient|
      Message.find(recipient[:message_id]).recipients.create(
        _id: recipient[:_id],
        talker_id: recipient[:talker_id],
        created_at: recipient[:created_at],
        updated_at: recipient[:updated_at],
        seen_at: recipient[:seen_at]
      )
    rescue Mongoid::Errors::DocumentNotFound
      next
    end
  end
end
