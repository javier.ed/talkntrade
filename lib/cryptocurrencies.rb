# frozen_string_literal: true

module Cryptocurrencies
  def self.ids(only_enabled: false)
    ids = []
    Settings.cryptocurrencies.map do |currency_id, currency_config|
      ids << currency_id.to_sym if !only_enabled || currency_config[:enable]
    end
    ids
  end

  def self.iso_codes(only_enabled: false)
    iso_codes = []
    Settings.cryptocurrencies.map do |currency_id, currency_config|
      iso_codes << Money::Currency.find(currency_id).iso_code if !only_enabled || currency_config[:enable]
    end
    iso_codes
  end
end
