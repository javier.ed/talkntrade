# frozen_string_literal: true

require 'net/http'

module Cryptocurrencies
  class Base
    def self.settings
      {
        enable: false,
        rpc_url: nil,
        tx_base_url: nil,
        allowed_transactions: {
          deposit: {
            min_amount: 0,
            min_fee: 0,
            fee_percentage: 0
          },
          transfer: {
            min_amount: 0,
            min_fee: 0,
            fee_percentage: 0
          },
          withdrawal: {
            min_amount: 0,
            min_fee: 0,
            fee_percentage: 0
          }
        }
      }
    end

    def self.call(name, **args)
      request_body = { method: name, params: args, id: :jsonrpc }.to_json
      response = JSON.parse http_request(request_body).body
      raise RPCError if response['error']

      response['result']
    end

    def self.http_request(body)
      rpc_url = URI.parse settings[:rpc_url]
      rpc_url.user ||= Rails.application.credentials.bitcoin_rpcuser
      rpc_url.password ||= Rails.application.credentials.bitcoin_rpcpassword
      request = Net::HTTP::Post.new rpc_url.request_uri
      request.basic_auth rpc_url.user, rpc_url.password
      request.content_type = 'application/json'
      request.body = body

      response = Net::HTTP.start rpc_url.hostname, rpc_url.port, use_ssl: (rpc_url.scheme == 'https') do |http|
        http.request(request)
      end

      yield response if block_given?
      response
    end

    def self.getaddressinfo(address)
      call :getaddressinfo, address: address
    end

    def self.getnewaddress(label)
      call :getnewaddress, label: label
    end

    def self.getwalletinfo
      call :getwalletinfo
    end

    def self.listtransactions(label = '*', count = 10, skip = 0)
      call :listtransactions, label: label, count: count, skip: skip
    end

    def self.sendtoaddress(address, amount)
      call :sendtoaddress, address: address, amount: amount
    end

    class RPCError < RuntimeError; end
  end
end
