# frozen_string_literal: true

module Cryptocurrencies
  class Bitcoin < Base
    def self.settings
      Settings.cryptocurrencies[:btc]
    end
  end
end
