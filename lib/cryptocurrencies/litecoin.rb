# frozen_string_literal: true

module Cryptocurrencies
  class Litecoin < Base
    def self.settings
      Settings.cryptocurrencies[:ltc]
    end
  end
end
