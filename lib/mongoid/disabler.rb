# frozen_string_literal: true

module Mongoid
  module Disabler
    extend ActiveSupport::Concern

    included do
      field :disabled_at, type: Time

      def disable
        update(disabled_at: Time.current) if enabled?
      end

      def enable
        update(disabled_at: nil) if disabled?
      end

      def enabled?
        disabled_at.nil?
      end

      def disabled?
        disabled_at.present?
      end

      def self.only_enabled
        where(disabled_at: nil)
      end
    end
  end
end
