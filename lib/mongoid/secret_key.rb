# frozen_string_literal: true

module Mongoid
  module SecretKey
    extend ActiveSupport::Concern

    module ClassMethods
      def has_secret_key(attribute = :secret_key) # rubocop:disable Naming/PredicateName
        field attribute, type: String

        define_method("regenerate_#{attribute}") { update! attribute => self.class.generate_secret_key }

        before_create { send("#{attribute}=", self.class.generate_secret_key) unless send("#{attribute}?") }
      end

      def generate_secret_key
        SecureRandom.base58 32
      end
    end
  end
end
