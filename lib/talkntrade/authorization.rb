# frozen_string_literal: true

module Talkntrade
  class Authorization
    attr_reader :request

    def initialize(request:)
      @request = request
    end

    # To get the current device
    #
    # @return [Device, nil] Return the current device or nil
    def current_device
      return @current_device if @current_device

      device_id = request.headers['X-Device-Id']
      device_token = request.headers['X-Device-Token']

      device = Device.only_unblocked.find(device_id) if device_id.present?

      device_payload = JWT.decode(device_token, device&.secret_key, true, verify_iat: true)[0]

      return nil unless validate_client_app(device, device_payload['app_id'])

      set_locale

      @current_session = find_current_session device, device_payload['session_id']

      @current_session&.update last_request_at: Time.current

      @current_device = device
    rescue Mongoid::Errors::DocumentNotFound, JWT::DecodeError
      nil
    end

    # To get the current session
    #
    # @return [Device::Session, nil] Return the current session or nil
    def current_session
      return nil unless current_device

      @current_session
    end

    # To get the current user
    #
    # @return [User, nil] Return the current user or nil
    def current_user
      return nil unless current_session

      return @current_user if @current_user

      @current_user = current_session.user
    end

    def device_authorized?
      current_device.present?
    end

    def user_authenticated?
      current_user.present? && current_session.present?
    end

    private

    # Validates the current client app
    #
    # @param device [Device] Current device
    # @param id [String] Client app ID
    # @return [Boolean] Return true if the client app is valid
    def validate_client_app(device, id)
      return false unless id

      return device.client_app.unexpired? if id == device.client_app.id.to_s

      client_app = ClientApp.find_unexpired(id)

      return false unless client_app

      device.update(:client_app, client_app)
    end

    def find_current_session(device, id)
      device.sessions.find_by(id: id, finished_at: nil) if id.present?
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end

    def set_locale
      locales = request.headers['Accept-Language']

      return if locales.blank?

      locales_array = locales.strip.split(/\s*,\s*/)

      selected_locales = locales_array.select { |l| Settings.locales.include?(l) }

      if !selected_locales.empty?
        I18n.locale = selected_locales[0]
      else
        selected_languages = locales_array.collect { |l| l.split(/-|_/)[0].downcase }
                                          .select { |l| Settings.locales.include?(l) }

        I18n.locale = selected_languages[0] unless selected_languages.empty?
      end
    end
  end
end
