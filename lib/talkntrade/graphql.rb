# frozen_string_literal: true

require 'active_support/concern'

module Talkntrade
  module Graphql
    module RequireDeviceAuthorization
      extend ActiveSupport::Concern

      included do
        def self.visible?(context)
          (super && context.current_device) || context.introspection?
        end

        def self.authorized?(object, context)
          (super && context.current_device)
        end
      end
    end

    module AuthenticateUser
      extend ActiveSupport::Concern

      included do
        def self.visible?(context)
          (super && context.current_user) || context.introspection?
        end

        def self.authorized?(object, context)
          (super && context.current_user)
        end
      end
    end

    module RequireNoAuthentication
      extend ActiveSupport::Concern

      included do
        def self.visible?(context)
          (super && context.current_device && !context.current_user) || context.introspection?
        end

        def self.authorized?(object, context)
          (super && context.current_device && !context.current_user)
        end
      end
    end

    module Scopes
      def self.scopes_valid?(context, scopes)
        (
          context&.current_device&.client_app&.scopes&.count&.positive? &&
          (
            context.current_device&.client_app&.scopes&.include?(:all) ||
            (context.current_device&.client_app&.scopes & scopes).count.positive?
          )
        ) || context.introspection?
      end

      extend ActiveSupport::Concern
      included do
        def self.scopes(*scopes)
          @scopes = scopes
        end

        def self.visible?(context)
          super && Scopes.scopes_valid?(context, @scopes || [])
        end

        def self.authorized?(object, context)
          super && Scopes.scopes_valid?(context, @scopes || [])
        end
      end
    end
  end
end
