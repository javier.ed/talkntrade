# frozen_string_literal: true

module Talkntrade
  module Routable
    extend ActiveSupport::Concern

    included do
      include Rails.application.routes.url_helpers

      def default_url_options
        Rails.application.config.action_mailer.default_url_options
      end
    end
  end
end
