# frozen_string_literal: true

FactoryBot.define do
  factory :client_app do
    description { 'Lorem ipsum' }
    scopes { %i[read_product] }
  end
end
