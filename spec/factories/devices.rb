# frozen_string_literal: true

FactoryBot.define do
  factory :device do
    client_app { create :client_app }
  end
end
