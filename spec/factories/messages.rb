# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    content { 'Foo bar baz' }
  end
end
