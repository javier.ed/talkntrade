# frozen_string_literal: true

FactoryBot.define do
  factory :reported_user do
    user { create :user }
    reported_user { create :user }
    subject { 'Some subject' } # rubocop:disable RSpec/EmptyLineAfterSubject
    message { 'Some message' }
    category { :other }
  end
end
