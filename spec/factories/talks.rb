# frozen_string_literal: true

FactoryBot.define do
  factory :talker do
    user { create :user_with_funds }
  end

  factory :talk do
    talkers { [build(:talker), build(:talker)] }
  end
end
