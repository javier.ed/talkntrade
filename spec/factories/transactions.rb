# frozen_string_literal: true

FactoryBot.define do
  factory :btc_deposit, class: 'Transaction' do
    category { :deposit }
    sequence(:txid) { |n| "0000000000000000000000000000000000000000000000000000000000000000#{n}" }
    total_amount { Money.from_amount(1, :btc) }

    factory :ltc_deposit do
      amount { Money.new(0, :ltc) }
      fee { Money.new(0, :ltc) }
      total_amount { Money.from_amount(1, :ltc) }
    end
  end
end
