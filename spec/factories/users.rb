# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "foo_#{n}" }
    sequence(:email) { |n| "foo#{n}@bar.baz" }
    password { '12345678' }
    password_confirmation { '12345678' }

    factory :user_with_funds do
      after(:create) do |user|
        create :btc_deposit, receiver: user.assets.find_by('balance.currency_iso' => 'BTC')
        create :ltc_deposit, receiver: user.assets.find_by('balance.currency_iso' => 'LTC')
      end
    end

    factory :user_with_verified_email do
      email_verified_at { Time.current }
    end

    factory :user_with_one_blocked_user do
      after(:create) do |user|
        user.blocked_users.create blocked_user: create(:user)
      end
    end

    factory :user_with_full_profile do
      after(:build) do |user|
        user.profile.display_name = 'Foo Bar'
        user.profile.first_name = 'Foo'
        user.profile.last_name = 'Bar'
        user.profile.birthdate = '1988-05-06'
        user.profile.country_gec = 'VE'
        user.profile.bio = 'Some bio'
        user.profile.avatar_image = Rack::Test::UploadedFile.new('spec/fixtures/image-256x256.png')
      end
    end
  end
end
