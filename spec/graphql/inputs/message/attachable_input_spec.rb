# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::Message::AttachableInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:type).of_type(GraphQL::Types::String) }
    it { expect(described_class).to have_graphql_argument(:amount).of_type(GraphQL::Types::Float) }
    it { expect(described_class).to have_graphql_argument(:currency_code).of_type(GraphQL::Types::String) }
  end
end
