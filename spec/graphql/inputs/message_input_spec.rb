# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::MessageInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:content).of_type(GraphQL::Types::String).required }
    it { expect(described_class).to have_graphql_argument(:attachable).of_type(Inputs::Message::AttachableInput) }
    it { expect(described_class).to have_graphql_argument(:talk_id).of_type(GraphQL::Types::ID).required }
  end
end
