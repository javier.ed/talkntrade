# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::OtpInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
    it { expect(described_class).to have_graphql_argument(:password).of_type(GraphQL::Types::String).required }
  end
end
