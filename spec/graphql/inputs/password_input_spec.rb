# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::PasswordInput, type: :graphql do
  describe 'arguments' do
    it do
      expect(described_class).to(
        have_graphql_arguments(:password, :password_confirmation).of_type(GraphQL::Types::String).required
      )
    end
  end
end
