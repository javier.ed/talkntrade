# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::ReportUserInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:user_id).of_type(GraphQL::Types::ID).required }

    it do
      expect(described_class).to(
        have_graphql_arguments(:category, :subject, :message).of_type(GraphQL::Types::String).required
      )
    end
  end
end
