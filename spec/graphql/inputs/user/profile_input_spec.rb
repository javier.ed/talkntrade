# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::User::ProfileInput, type: :graphql do
  describe 'arguments' do
    it do
      expect(described_class).to(
        have_graphql_arguments(:display_name, :first_name, :last_name, :country_gec, :bio)
        .of_type(GraphQL::Types::String).required
      )
    end

    it do
      expect(described_class).to have_graphql_argument(:birthdate).of_type(GraphQL::Types::ISO8601DateTime)
    end

    it { expect(described_class).to have_graphql_argument(:avatar_image).of_type(ApolloUploadServer::Upload) }
    it { expect(described_class).to have_graphql_argument(:remove_avatar_image).of_type(GraphQL::Types::Boolean) }
  end
end
