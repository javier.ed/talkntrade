# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::UserInput, type: :graphql do
  describe 'arguments' do
    it do
      expect(described_class).to(
        have_graphql_argument(:username, :email, :password, :password_confirmation).of_type(GraphQL::Types::String)
          .required
      )
    end
  end
end
