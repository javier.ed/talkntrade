# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::WithdrawInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:amount).of_type(GraphQL::Types::Float).required }

    it do
      expect(described_class).to(
        have_graphql_argument(:currency_iso_code, :receiver_address).of_type(GraphQL::Types::String).required
      )
    end
  end
end
