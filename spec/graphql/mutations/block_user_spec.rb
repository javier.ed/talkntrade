# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::BlockUser, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:user_id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:user_to_block) { create :user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }

    context 'when all data is valid' do
      it 'block user' do
        expect(mutation.resolve(user_id: user_to_block.id)).to(
          include success: true, message: t('mutations.block_user.success')
        )
      end
    end

    context 'when user_id is invalid' do
      it 'fail to block user' do
        expect(mutation.resolve(user_id: 'wrong id')).to include success: false, message: t('mutations.block_user.fail')
      end
    end

    context 'when user_id is already blocked' do
      it 'fail to block user' do
        user.blocked_users.create blocked_user: user_to_block
        expect(mutation.resolve(user_id: user_to_block.id)).to(
          include success: false, message: t('mutations.block_user.fail')
        )
      end
    end
  end
end
