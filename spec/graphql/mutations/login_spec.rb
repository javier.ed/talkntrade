# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::Login, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_arguments(:login, :password).of_type(GraphQL::Types::String).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:user).of_type(Types::UserType).allow_null }
    it { expect(described_class).to have_graphql_field(:session_id).of_type(GraphQL::Types::ID).allow_null }
  end

  describe '#resolve' do
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context }
    let(:password) { '12345678' }
    let(:user) { create :user, password: password, password_confirmation: password }

    context 'when attributes are valid' do
      it 'login user' do
        expect(mutation.resolve(login: user.username, password: password)).to(
          include success: true, message: t('mutations.login.success')
        )
      end
    end

    context 'when password is invalid' do
      it 'fails to login user' do
        expect(mutation.resolve(login: user.username, password: 'wrong password')).to(
          include success: false, message: t('mutations.login.fail')
        )
      end
    end

    context 'when the user is disabled' do
      it 'fails to login user' do
        user.disable
        expect(mutation.resolve(login: user.username, password: password)).to(
          include success: false, message: t('mutations.login.fail')
        )
      end
    end
  end
end
