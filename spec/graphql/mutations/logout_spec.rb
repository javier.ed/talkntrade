# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::Logout, type: :graphql do
  describe '#resolve' do
    let(:user) { create :user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }

    it 'logouts user' do
      expect(mutation.resolve).to include success: true, message: t('mutations.logout.success')
    end
  end
end
