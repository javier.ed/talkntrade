# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::MarkSeenMessage, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:talk) { create :talk }
    let(:sender) { talk.talkers.first }
    let(:receiver) { talk.talkers.last }
    let(:message) { create :message, talk: talk, talker: sender }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: receiver.user) }

    context 'when ID is valid' do
      it 'mark seen message' do
        expect(mutation.resolve(id: message.id)).to include success: true
      end
    end

    context 'when ID is invalid' do
      it 'mark unseen message' do
        expect(mutation.resolve(id: 'wrong id')).to(
          include success: false, message: t('mutations.mark_seen_message.fail')
        )
      end
    end
  end
end
