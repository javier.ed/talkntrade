# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::RegisterDevice, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:app_id).of_type(GraphQL::Types::ID).required }
    it { expect(described_class).to have_graphql_argument(:app_token).of_type(GraphQL::Types::String).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:device_id).of_type(GraphQL::Types::ID).allow_null }

    it do
      expect(described_class).to(
        have_graphql_field(:device_secret_key, :app_token).of_type(GraphQL::Types::String).allow_null
      )
    end
  end

  describe '#resolve' do
    let(:client_app) { create :client_app }
    let(:app_token) { JWT.encode({}, client_app.secret_key) }
    let(:mutation) do
      described_class.new(
        object: nil, field: nil, context: OpenStruct.new(request: OpenStruct.new(remote_ip: '127.0.0.1'))
      )
    end

    context 'when client app is valid' do
      let(:data) { mutation.resolve(app_id: client_app.id, app_token: app_token) }

      it 'register device' do
        expect(data).to include success: true, message: t('mutations.register_device.success')
      end

      it 'returns device data in a JWT encoded with the app secret key' do
        expect { JWT.decode(data[:app_token], client_app.secret_key) }.not_to raise_error
      end
    end

    context 'when client app is invalid' do
      it 'fails to register device' do
        expect(mutation.resolve(app_id: client_app.id, app_token: 'wrong data')).to(
          include success: false, message: t('mutations.register_device.fail')
        )
      end
    end
  end
end
