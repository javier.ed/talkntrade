# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::ReportUser, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::ReportUserInput).required }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:reported_user) { create :user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }
    let :attributes do
      OpenStruct.new(
        user_id: reported_user.id,
        category: Settings.reported_user_categories.sample,
        subject: 'Some subject',
        message: 'Some message'
      )
    end

    context 'when all data is valid' do
      it 'reports an user' do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.report_user.success')
        )
      end
    end
  end
end
