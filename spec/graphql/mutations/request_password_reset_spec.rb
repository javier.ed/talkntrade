# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::RequestPasswordReset, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:login).of_type(GraphQL::Types::String).required }
  end

  describe '#resolve' do
    let(:user) { create :user_with_verified_email }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }

    context 'when login is valid' do
      it 'send a password reset token' do
        expect(mutation.resolve(login: user.username)).to(
          include success: true, message: t('mutations.request_password_reset.success')
        )
      end
    end

    context 'when login is invalid' do
      it 'fail to send a password reset token' do
        expect(mutation.resolve(login: 'wrong login')).to(
          include success: false, message: t('mutations.request_password_reset.fail')
        )
      end
    end
  end
end
