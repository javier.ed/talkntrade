# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::ResetPassword, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:otp).of_type(Inputs::OtpInput).required }
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::PasswordInput).required }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context }
    let(:otp) { user.request_password_reset }
    let(:wrong_otp) { OpenStruct.new id: 'wrong id', password: 'wrong password' }
    let(:attributes) { OpenStruct.new password: '87654321', password_confirmation: '87654321' }

    context 'when otp is valid' do
      it 'changes password' do
        expect(mutation.resolve(otp: otp, attributes: attributes)).to(
          include success: true, message: t('mutations.reset_password.success')
        )
      end
    end

    context 'when otp is invalid' do
      it 'fails to change password' do
        expect(mutation.resolve(otp: wrong_otp, attributes: attributes)).to(
          include success: false, message: t('mutations.reset_password.fail')
        )
      end
    end
  end
end
