# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::SendEmailVerificationToken, type: :graphql do
  describe '#resolve' do
    let(:user) { create :user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }

    context 'when email isn\'t verified' do
      it 'send the verification token' do
        expect(mutation.resolve).to include success: true, message: t('mutations.send_email_verification_token.success')
      end
    end

    context 'when emails is already verified' do
      it 'fails to send the verification token' do
        user.update email_verified_at: Time.current
        expect(mutation.resolve).to include success: false, message: t('mutations.send_email_verification_token.fail')
      end
    end
  end
end
