# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::SendMessage, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::MessageInput).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:sent_message).of_type(Types::MessageType).allow_null }
  end

  describe '#resolve' do
    let(:talk) { create :talk }
    let(:user) { talk.talkers.first.user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }
    let(:attributes) { OpenStruct.new content: 'Loren ipsum...', talk_id: talk.id }

    context 'when attributes valid' do
      it 'sends a message' do
        expect(mutation.resolve(attributes: attributes)).to include success: true
      end
    end

    context 'when talk is invalid' do
      it 'fails to send message' do
        attributes.talk_id = 'wrong id'
        expect(mutation.resolve(attributes: attributes)).to include success: false
      end
    end

    context 'when is sending money' do
      it 'sends money' do
        attributes.attachable = OpenStruct.new type: 'transaction', amount: 0.01, currency_code: 'BTC'
        expect(mutation.resolve(attributes: attributes)).to include success: true
      end
    end

    context 'when is sending more money than what is available' do
      it 'fails to send money' do
        attributes.attachable = OpenStruct.new type: 'transaction', amount: 1, currency_code: 'BTC'
        expect(mutation.resolve(attributes: attributes)).to include success: false
      end
    end

    context 'when the other user is disabled' do
      it 'fails to send message' do
        talk.talkers.last.user.disable
        expect(mutation.resolve(attributes: attributes)).to include success: false
      end
    end
  end
end
