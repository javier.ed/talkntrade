# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::StartTalk, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:talk).of_type(Types::TalkType).allow_null }
  end

  describe '#resolve' do
    let(:current_user) { create :user }
    let(:user) { create :user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: current_user) }

    context 'when user id is valid' do
      it 'start a talk' do
        expect(mutation.resolve(id: user.id)).to include success: true
      end
    end

    context 'when user id invalid' do
      it 'fails to start a talk' do
        expect(mutation.resolve(id: 'wrong id')).to include success: false
      end
    end

    context 'when the other user is blocked' do
      it 'fails to start a talk' do
        current_user.blocked_users.create blocked_user: user
        expect(mutation.resolve(id: user.id)).to include success: false
      end
    end

    context 'when the current user is blocked' do
      it 'fails to start a talk' do
        user.blocked_users.create blocked_user: current_user
        expect(mutation.resolve(id: user.id)).to include success: false
      end
    end
  end
end
