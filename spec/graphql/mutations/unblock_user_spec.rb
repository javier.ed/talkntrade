# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::UnblockUser, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:user_id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:blocked_user) { create :user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }

    before { user.blocked_users.create blocked_user: blocked_user }

    context 'when all data is valid' do
      it 'unblock user' do
        expect(mutation.resolve(user_id: blocked_user.id)).to(
          include success: true, message: t('mutations.unblock_user.success')
        )
      end
    end

    context 'when user_id is invalid' do
      it 'fail to unblock user' do
        expect(mutation.resolve(user_id: 'wrong id')).to(
          include success: false, message: t('mutations.unblock_user.fail')
        )
      end
    end

    context 'when user_id is not blocked' do
      it 'fail to unblock user' do
        user.blocked_users.find_by(blocked_user: blocked_user).destroy
        expect(mutation.resolve(user_id: blocked_user.id)).to(
          include success: false, message: t('mutations.unblock_user.fail')
        )
      end
    end
  end
end
