# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::UpdateProfile, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::User::ProfileInput).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:profile).of_type(Types::User::ProfileType).allow_null }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }
    let :attributes do
      OpenStruct.new(
        display_name: 'Foo B. Baz', first_name: 'Foo Bar', last_name: 'Baz', birthdate: '1970-01-01', country_gec: 'VE',
        bio: 'Lorem Ipsum'
      )
    end

    it 'updates profile' do
      expect(mutation.resolve(attributes: attributes)).to(
        include success: true, message: t('mutations.update_profile.success')
      )
    end
  end
end
