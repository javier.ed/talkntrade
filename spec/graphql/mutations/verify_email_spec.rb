# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::VerifyEmail, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:token).of_type(GraphQL::Types::String).required }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }

    context 'when the token is valid' do
      it 'verifies the emails address' do
        expect(mutation.resolve(token: user.email_verification_token)).to(
          include success: true, message: t('mutations.verify_email.success')
        )
      end
    end

    context 'when the token is invalid' do
      it 'fails to verify the email address' do
        expect(mutation.resolve(token: 'wrong token')).to(
          include success: false, message: t('mutations.verify_email.fail')
        )
      end
    end
  end
end
