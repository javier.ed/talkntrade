# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::Withdraw, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::WithdrawInput).required }
  end

  describe '#resolve' do
    let(:user) { create :user_with_funds }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }
    let(:attributes) do
      OpenStruct.new currency_iso_code: 'BTC', amount: 0.01, receiver_address: random_string(35)
    end

    context 'when all data is valid' do
      it 'withdraw funds' do
        expect(mutation.resolve(attributes: attributes)).to include(
          success: true, message: t('mutations.withdraw.success')
        )
      end
    end

    context 'when funds are not enough' do
      it 'fails to withdraw funds' do
        attributes.amount = 1
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.withdraw.fail')
        )
      end
    end
  end
end
