# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TalkntradeSchema do
  it 'has a base query' do
    expect(described_class.query).to eq Types::QueryType
  end

  it 'has a base mutation' do
    expect(described_class.mutation).to eq Types::MutationType
  end

  it 'has a base subscription' do
    expect(described_class.subscription).to eq Types::SubscriptionType
  end
end
