# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::AssetType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field(:id) }
    it { expect(described_class).to have_graphql_field(:balance).of_type(GraphQL::Types::Float).allow_null }
    it { expect(described_class).to have_graphql_field(:currency).of_type(Types::CurrencyType).allow_null }
    it { expect(described_class).to have_graphql_field(:primary_address).of_type(GraphQL::Types::String).allow_null }
    it { expect(described_class).to have_graphql_field(:transactions).of_type([Types::TransactionType]).allow_null }
  end
end
