# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::Currency::AllowedTransactionType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }

    it do
      expect(described_class).to(
        have_graphql_fields(:min_amount, :min_fee, :fee_percentage).of_type(GraphQL::Types::Float).allow_null
      )
    end
  end
end
