# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::CurrencyType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field(:id) }

    it do
      expect(described_class).to(
        have_graphql_fields(:name, :iso_code, :symbol, :subunit).of_type(GraphQL::Types::String).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:exponent).of_type(GraphQL::Types::Int).allow_null }

    it do
      expect(described_class).to(
        have_graphql_field(:allowed_transactions).of_type([Types::Currency::AllowedTransactionType]).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_field(:allowed_transaction).of_type(Types::Currency::AllowedTransactionType).allow_null
      )
    end
  end
end
