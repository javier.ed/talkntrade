# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::ImageType, type: :graphql do
  describe 'fields' do
    it do
      expect(described_class).to(
        have_graphql_field(:url, :file_name, :file_type).of_type(GraphQL::Types::String).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_field(:size, :width, :height, :orientation).of_type(GraphQL::Types::Int).allow_null
      )
    end
  end
end
