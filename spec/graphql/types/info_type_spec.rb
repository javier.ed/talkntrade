# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::InfoType, type: :graphql do
  describe 'fields' do
    it do
      expect(described_class).to(
        have_graphql_field(:server_version, :client_status).of_type(GraphQL::Types::String).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_field(:server_date, :client_expires_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:scopes).of_type([GraphQL::Types::String]).allow_null }
  end
end
