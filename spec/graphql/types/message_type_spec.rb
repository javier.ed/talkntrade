# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::MessageType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field(:id) }

    it do
      expect(described_class).to(
        have_graphql_field(:created_at, :updated_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:content).of_type(GraphQL::Types::String).allow_null }

    it do
      expect(described_class).to have_graphql_field(:attachable).of_type(Types::Message::AttachableType).allow_null
    end

    it { expect(described_class).to have_graphql_field(:talker).of_type(Types::TalkerType).allow_null }
    it { expect(described_class).to have_graphql_field(:talk).of_type(Types::TalkType).allow_null }
    it { expect(described_class).to have_graphql_fields(:seen, :received).of_type(GraphQL::Types::Boolean).allow_null }
  end
end
