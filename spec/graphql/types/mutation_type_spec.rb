# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::MutationType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:register_device).with_mutation(Mutations::RegisterDevice) }

    it { expect(described_class).to have_graphql_field(:login).with_mutation(Mutations::Login) }
    it { expect(described_class).to have_graphql_field(:logout).with_mutation(Mutations::Logout) }
    it { expect(described_class).to have_graphql_field(:register).with_mutation(Mutations::Register) }

    it { expect(described_class).to have_graphql_field(:update_profile).with_mutation(Mutations::UpdateProfile) }

    it { expect(described_class).to have_graphql_field(:change_email).with_mutation(Mutations::ChangeEmail) }

    it do
      expect(described_class).to(
        have_graphql_field(:send_email_verification_token).with_mutation(Mutations::SendEmailVerificationToken)
      )
    end

    it { expect(described_class).to have_graphql_field(:verify_email).with_mutation(Mutations::VerifyEmail) }

    it do
      expect(described_class).to(
        have_graphql_field(:request_password_reset).with_mutation(Mutations::RequestPasswordReset)
      )
    end

    it { expect(described_class).to have_graphql_field(:reset_password).with_mutation(Mutations::ResetPassword) }
    it { expect(described_class).to have_graphql_field(:change_password).with_mutation(Mutations::ChangePassword) }

    it { expect(described_class).to have_graphql_field(:mark_seen_message).with_mutation(Mutations::MarkSeenMessage) }
    it { expect(described_class).to have_graphql_field(:send_message).with_mutation(Mutations::SendMessage) }
    it { expect(described_class).to have_graphql_field(:start_talk).with_mutation(Mutations::StartTalk) }

    it { expect(described_class).to have_graphql_field(:withdraw).with_mutation(Mutations::Withdraw) }

    it { expect(described_class).to have_graphql_field(:block_user).with_mutation(Mutations::BlockUser) }
    it { expect(described_class).to have_graphql_field(:unblock_user).with_mutation(Mutations::UnblockUser) }

    it { expect(described_class).to have_graphql_field(:report_user).with_mutation(Mutations::ReportUser) }
  end
end
