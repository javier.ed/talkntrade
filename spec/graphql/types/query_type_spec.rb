# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::QueryType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:info).of_type(Types::InfoType).allow_null }
    it { expect(described_class).to have_graphql_field(:currencies).of_type([Types::CurrencyType]).allow_null }
    it { expect(described_class).to have_graphql_field(:currency).of_type(Types::CurrencyType).allow_null }

    it { expect(described_class).to have_graphql_field(:current_user).of_type(Types::UserType).allow_null }

    it { expect(described_class).to have_graphql_field(:users).of_type([Types::UserType]).allow_null }
    it { expect(described_class).to have_graphql_field(:user).of_type(Types::UserType).allow_null }

    it { expect(described_class).to have_graphql_field(:talks).of_type([Types::TalkType]).allow_null }

    it { expect(described_class).to have_graphql_field(:talk).of_type(Types::TalkType).allow_null }

    it { expect(described_class).to have_graphql_field(:message).of_type(Types::MessageType).allow_null }

    it { expect(described_class).to have_graphql_field(:assets).of_type([Types::AssetType]).allow_null }

    it { expect(described_class).to have_graphql_field(:asset).of_type(Types::AssetType).allow_null }
  end
end
