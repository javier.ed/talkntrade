# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::SubscriptionType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:new_message).of_type(Types::MessageType) }
  end
end
