# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::TalkType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }

    it do
      expect(described_class).to(
        have_graphql_fields(:created_at, :updated_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:name).of_type(GraphQL::Types::String).allow_null }
    it { expect(described_class).to have_graphql_field(:talkers).of_type([Types::TalkerType]).allow_null }

    it do
      expect(described_class).to(
        have_graphql_field(:last_message_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:unseen_count).of_type(GraphQL::Types::Int).allow_null }
    it { expect(described_class).to have_graphql_field(:messages).of_type([Types::MessageType]).allow_null }
  end
end
