# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::TransactionType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }

    it do
      expect(described_class).to have_graphql_field(:created_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
    end

    it { expect(described_class).to have_graphql_field(:number).of_type(GraphQL::Types::Int).allow_null }
    it { expect(described_class).to have_graphql_fields(:amount, :fee).of_type(GraphQL::Types::Float).allow_null }
    it { expect(described_class).to have_graphql_field(:currency).of_type(Types::CurrencyType).allow_null }

    it do
      expect(described_class).to(
        have_graphql_fields(:txid, :receiver_address, :tx_url).of_type(GraphQL::Types::String).allow_null
      )
    end
  end
end
