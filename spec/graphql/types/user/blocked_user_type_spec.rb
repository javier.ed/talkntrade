# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::User::BlockedUserType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }

    it do
      expect(described_class).to(
        have_graphql_fields(:created_at, :updated_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:blocked_user).of_type(Types::UserType).allow_null }
  end
end
