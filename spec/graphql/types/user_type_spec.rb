# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::UserType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field(:id) }

    it do
      expect(described_class).to(
        have_graphql_fields(:created_at, :updated_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:username, :email).of_type(GraphQL::Types::String).allow_null }
    it { expect(described_class).to have_graphql_field(:email_verified).of_type(GraphQL::Types::Boolean).allow_null }

    it { expect(described_class).to have_graphql_field(:profile).of_type(Types::User::ProfileType).allow_null }

    it { expect(described_class).to have_graphql_field(:talks).of_type([Types::TalkType]).allow_null }

    it do
      expect(described_class).to have_graphql_field(:blocked_users).of_type([Types::User::BlockedUserType]).allow_null
    end

    it { expect(described_class).to have_graphql_field(:webclient_url).of_type(GraphQL::Types::String).allow_null }
  end
end
