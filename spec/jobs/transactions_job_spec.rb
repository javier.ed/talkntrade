# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TransactionsJob, type: :job do
  describe '#perform_later' do
    it 'analizes transactions' do
      ActiveJob::Base.queue_adapter = :test
      expect { described_class.perform_later }.to have_enqueued_job
    end
  end
end
