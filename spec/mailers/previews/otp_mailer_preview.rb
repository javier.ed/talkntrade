# frozen_string_literal: true

class OtpMailerPreview < ActionMailer::Preview
  def password_mail
    OtpMailer.with(to: 'foo@bar.baz', username: 'some_guy', password: 'L0R3M1P5UM', action: :reset_password)
             .password_mail
  end
end
