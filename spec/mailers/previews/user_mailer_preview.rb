# frozen_string_literal: true

class UserMailerPreview < ActionMailer::Preview
  def email_verification_mail
    UserMailer.with(to: 'foo@bar.baz', username: 'some_guy', email_verification_token: 'L0R3M1P5UM')
              .email_verification_mail
  end
end
