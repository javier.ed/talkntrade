# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  let(:email) { 'foo@bar.baz' }
  let(:username) { 'some_guy' }
  let(:email_verification_token) { 'L0R3M1P5UM' }

  describe 'email verification mail' do
    let(:mail) do
      described_class.with(to: email, username: username, email_verification_token: email_verification_token)
                     .email_verification_mail
    end

    it { expect(mail.to).to eq [email] }
    it { expect(mail.subject).to eq t('user_mailer.email_verification_mail.subject') }
    it { expect(mail.body.encoded).to match t('user_mailer.email_verification_mail.first_paragraph') }
  end
end
