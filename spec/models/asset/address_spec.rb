# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Asset::Address, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:address).of_type(String) }
    it { is_expected.to have_field(:label).of_type(Symbol) }
  end

  describe 'associations' do
    it { is_expected.to be_embedded_in :asset }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :address }
    it { is_expected.to validate_inclusion_of(:label).to_allow(:primary, :backup) }
  end
end
