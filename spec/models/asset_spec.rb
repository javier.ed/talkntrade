# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Asset, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:balance).of_type(Money) }
  end

  describe 'associations' do
    it { is_expected.to belong_to :user }
    it { is_expected.to embed_many(:addresses).of_type(Asset::Address) }
    it { is_expected.to have_many(:sent_transactions).of_type(Transaction).with_dependent(:restrict_with_exception) }
  end
end
