# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ClientApp, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:expires_at).of_type(Time) }
    it { is_expected.to have_field(:description, :secret_key).of_type(String) }
    it { is_expected.to have_field(:scopes).of_type(Array) }
  end

  describe 'associations' do
    it { is_expected.to have_many(:devices).with_dependent(:restrict_with_exception) }
  end

  describe '#jwt_decode' do
    let(:client_app) { create :client_app }
    let(:jwt) { JWT.encode({}, client_app.secret_key) }

    context 'when JWT is valid' do
      it 'returns a decoded JWT' do
        expect(client_app.jwt_decode(jwt)).to be_truthy
      end
    end

    context 'when JWT is invalid' do
      it 'returns nil' do
        expect(client_app.jwt_decode('wrong.token')).to be_nil
      end
    end
  end

  describe '#jwt_encode' do
    let(:client_app) { create :client_app }

    context 'when payload is valid' do
      it 'returns a decoded JWT' do
        expect(client_app.jwt_encode({})).to be_truthy
      end
    end
  end
end
