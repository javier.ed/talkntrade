# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Device::LoginAttempt, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'associations' do
    it { is_expected.to be_embedded_in :device }
    it { is_expected.to belong_to :user }
  end

  describe 'fields' do
    it { is_expected.to have_field(:counter).of_type(Integer) }
  end

  describe 'validations' do
    it do
      expect(described_class).to(
        validate_numericality_of(:counter).greater_than_or_equal_to(0)
      )
    end
  end

  describe '.create' do
    let(:user) { create :user }
    let(:device) { create :device }

    context 'when the counter is less than the maximum' do
      it 'doesn\'t block the device' do
        device.login_attempts.create(user: user)
        device.reload
        expect(device.blocked_at).to be_nil
      end
    end

    context 'when the counter equal to the maximum' do
      it 'block the device' do
        device.login_attempts.create(user: user, counter: Settings.max_login_attempts)
        device.reload
        expect(device.blocked_at).to be_truthy
      end
    end
  end

  describe '#increment_counter' do
    let(:user) { create :user }
    let(:device) { create :device }
    let(:login_attempt) { device.login_attempts.create(user: user) }

    before do
      login_attempt.increment_counter
      login_attempt.reload
    end

    it 'increment counter' do
      expect(login_attempt.counter).to eq 1
    end

    it 'reset counter' do
      login_attempt.reset_counter
      login_attempt.reload
      expect(login_attempt.counter).to eq 0
    end
  end
end
