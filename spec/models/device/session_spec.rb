# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Device::Session, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:last_request_at).of_type(Time) }
    it { is_expected.to have_field(:finished_at).of_type(Time) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user).of_type(User) }
    it { is_expected.to be_embedded_in :device }
  end
end
