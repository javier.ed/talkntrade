# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Device, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:secret_key).of_type(String) }
    it { is_expected.to have_field(:blocked_at).of_type(Time) }
  end

  describe 'associations' do
    it { is_expected.to belong_to :client_app }
    it { is_expected.to embed_many(:sessions).of_type(Device::Session) }
    it { is_expected.to embed_many(:login_attempts).of_type(Device::LoginAttempt) }
  end

  describe '.only_unblocked' do
    let!(:device) { create :device }

    context 'when the device is unblocked' do
      it 'counts one' do
        expect(described_class.only_unblocked.count).to eq 1
      end
    end

    context 'when the device is blocked' do
      it 'counts zero' do
        device.set(blocked_at: Time.current)
        expect(described_class.only_unblocked.count).to eq 0
      end
    end
  end
end
