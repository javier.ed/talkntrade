# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Message::Recipient, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_fields(:received_at, :seen_at).of_type(Time) }
  end

  describe 'associations' do
    it { is_expected.to be_embedded_in :message }
    it { is_expected.to belong_to :talker }
  end

  describe '#mark_previous' do
    let(:talk) { create :talk }
    let(:sender) { talk.talkers.first }
    let(:receiver) { talk.talkers.last }
    let!(:previous_message) { create :message, talker: sender, talk: talk }
    let!(:reply_message) { create :message, talker: receiver, talk: talk }
    let!(:current_message) { create :message, talker: sender, talk: talk }
    let!(:next_message) { create :message, talker: sender, talk: talk }

    context 'when there is one previous unseen message' do
      before do
        current_message.recipients.find_by(talker: receiver).update(seen_at: Time.current)
      end

      it 'mark previous message as seen' do
        previous_message.reload
        expect(previous_message.recipients.find_by(talker: receiver).seen_at).to be_truthy
      end

      it 'mark current message as seen' do
        current_message.reload
        expect(current_message.recipients.find_by(talker: receiver).seen_at).to be_truthy
      end

      it 'doesn\'t mark reply message es seen' do
        reply_message.reload
        expect(reply_message.recipients.find_by(talker: sender).seen_at).to be_nil
      end

      it 'doesn\'t mark next message as seen' do
        next_message.reload
        expect(next_message.recipients.find_by(talker: receiver).seen_at).to be_nil
      end
    end
  end
end
