# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Message, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:content).of_type(String) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:talker) }
    it { is_expected.to belong_to(:talk) }

    it { is_expected.to belong_to(:attachable) }
    it { is_expected.to embed_many(:recipients).of_type(Message::Recipient) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :content }
    it { is_expected.to validate_inclusion_of(:attachable_type).to_allow('Transaction') }
  end
end
