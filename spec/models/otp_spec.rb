# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Otp, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:action).of_type(Symbol) }
    it { is_expected.to have_field(:password_digest).of_type(String) }
    it { is_expected.to have_field(:used_at).of_type(Time) }
    it { is_expected.to have_field(:attempts).of_type(Integer).with_default_value_of(0) }
  end

  describe 'associations' do
    it { is_expected.to belong_to :user }
  end

  describe 'validations' do
    it { is_expected.to validate_inclusion_of(:action).to_allow(:reset_password) }
    it { is_expected.to validate_numericality_of(:attempts).greater_than_or_equal_to(0).less_than_or_equal_to(3) }
  end
end
