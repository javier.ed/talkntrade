# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReportedUser, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:category).of_type(Symbol) }
    it { is_expected.to have_field(:subject, :message).of_type(String) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user).as_inverse_of(:reported_users) }
    it { is_expected.to belong_to(:reported_user).of_type(User).as_inverse_of(:reports_received) }
  end

  describe 'validations' do
    it { is_expected.to validate_inclusion_of(:category).to_allow(Settings.reported_user_categories) }
    it { is_expected.to validate_presence_of(:subject) }
    it { is_expected.to validate_length_of(:subject).within(5..256) }
    it { is_expected.to validate_presence_of(:message) }
    it { is_expected.to validate_uniqueness_of(:reported_user_id).scoped_to(:user_id, :category) }
  end

  describe '#save' do
    let(:reported_user) { build :reported_user }

    context 'when all data is valid' do
      it 'reports an user' do
        expect(reported_user.save).to eq true
      end
    end

    context 'when a similar report has been made' do
      let :reported_user1 do
        reported_user.save
        build :reported_user, user: reported_user.user, reported_user: reported_user.reported_user
      end

      it 'fails to report user' do
        expect(reported_user1.save).to eq false
      end
    end

    context 'when user and reported user are the same' do
      let(:user) { create :user }
      let(:reported_user) { build :reported_user, user: user, reported_user: user }

      it 'fails to report user' do
        expect(reported_user.save).to eq false
      end
    end
  end
end
