# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Talk, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:last_message_at).of_type(Time) }
  end

  describe 'associations' do
    it { is_expected.to have_many(:messages).with_dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:talkers).with_dependent(:restrict_with_exception) }
    it { is_expected.to accept_nested_attributes_for(:talkers) }
  end

  describe 'validations' do
    it { is_expected.to validate_length_of(:talkers).on(:create) }
    it { is_expected.to validate_associated(:talkers) }
  end

  describe '.count' do
    context 'when there is no talks' do
      it 'counts zero' do
        expect(described_class.count).to eq 0
      end
    end

    context 'when there is one talk' do
      it 'counts one' do
        create :talk
        expect(described_class.count).to eq 1
      end
    end
  end

  describe '.create' do
    context 'when all data is valid' do
      let(:talk) { build :talk }

      it 'creates talk' do
        expect(talk.save).to eq true
      end
    end

    context 'when there is no talkers' do
      let(:talk) { build :talk, talkers: [] }

      it 'fails to create talk' do
        expect(talk.save).to eq false
      end
    end

    context 'when there is only one talker' do
      let(:talk) { build :talk, talkers: [build(:talker)] }

      it 'fails to create talk' do
        expect(talk.save).to eq false
      end
    end

    context 'when there is more than two talkers' do
      let(:talk) { build :talk, talkers: [build(:talker), build(:talker), build(:talker)] }

      it 'fails to create talk' do
        expect(talk.save).to eq false
      end
    end

    context 'when the talkers are the same user' do
      let(:user) { create :user }
      let(:talk) { build :talk, talkers: [build(:talker, user: user), build(:talker, user: user)] }

      it 'fails to create talk' do
        expect(talk.save).to eq false
      end
    end

    context 'when users already have a talk' do
      let(:first_talk) { create :talk }
      let(:user1) { first_talk.talkers.first.user }
      let(:user2) { first_talk.talkers.last.user }
      let(:talk) { build :talk, talkers: [build(:talker, user: user1), build(:talker, user: user2)] }

      it 'fails to create talk' do
        expect(talk.save).to eq false
      end
    end
  end

  describe '#name_for_talker' do
    let(:talk) { create :talk }
    let(:first_talker) { talk.talkers.first }
    let(:last_talker) { talk.talkers.last }

    context 'when all data is valid' do
      it 'return the name of the talk' do
        expect(talk.name_for_talker(first_talker)).to eq last_talker.user.username
      end
    end

    context 'when the talker is invalid' do
      let(:other_talk) { create :talk }
      let(:other_talker) { other_talk.talkers.first }

      it 'return nil' do
        expect(talk.name_for_talker(other_talker)).to be_nil
      end
    end
  end
end
