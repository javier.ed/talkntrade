# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Talker, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'associations' do
    it { is_expected.to belong_to :talk }
    it { is_expected.to belong_to :user }
    it { is_expected.to have_many(:messages).with_dependent(:restrict_with_exception) }
  end

  describe 'validations' do
    it { is_expected.to validate_uniqueness_of(:user_id).scoped_to(:talk_id) }
  end
end
