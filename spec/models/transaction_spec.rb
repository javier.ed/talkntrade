# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Transaction, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:number).of_type(Integer) }
    it { is_expected.to have_field(:category).of_type(Symbol) }
    it { is_expected.to have_fields(:amount, :fee).of_type(Money).with_default_value_of(0) }
    it { is_expected.to have_field(:txid).of_type(String) }
    it { is_expected.to have_field(:receiver_address_id).of_type(BSON::ObjectId) }
    it { is_expected.to have_field(:receiver_address).of_type(String) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:sender).of_type(Asset) }
    it { is_expected.to belong_to(:receiver).of_type(Asset) }
  end

  describe 'validations' do
    it { is_expected.to validate_inclusion_of(:category).to_allow(:deposit, :withdrawal, :transfer) }
    it { is_expected.to validate_uniqueness_of(:txid).case_insensitive }
    it { is_expected.to validate_presence_of(:sender) }
    it { is_expected.to validate_presence_of(:receiver) }
    it { is_expected.to validate_presence_of(:receiver_address) }
    it { is_expected.to validate_length_of(:receiver_address).within(26..35) }
  end

  describe '.register_deposit' do
    let(:user) { create :user }
    let(:btc_deposit) { build :btc_deposit, receiver: user.assets.find_by('balance.currency_iso' => 'BTC') }
    let!(:ltc_deposit) { build :ltc_deposit, receiver: user.assets.find_by('balance.currency_iso' => 'LTC') }

    context 'when all data is valid' do
      it 'register btc deposit' do
        described_class.register_deposit(
          txid: btc_deposit.txid, receiver_address: btc_deposit.receiver_address, total_amount: 1, currency_iso: 'BTC'
        )
        expect(described_class.count).to eq 1
      end

      it 'register ltc deposit' do
        described_class.register_deposit(
          txid: ltc_deposit.txid, receiver_address: ltc_deposit.receiver_address, total_amount: 1, currency_iso: 'LTC'
        )
        expect(described_class.count).to eq 1
      end
    end

    context 'when receiver address is invalid' do
      it 'fail to register deposit' do
        described_class.register_deposit(
          txid: btc_deposit.txid, receiver_address: 'wrong address', total_amount: 1, currency_iso: 'BTC'
        )
        expect(described_class.count).to eq 0
      end
    end

    context 'when currency is disabled' do
      it 'fail to register deposit' do
        Cryptocurrencies::Litecoin.settings[:enable] = false
        described_class.register_deposit(
          txid: ltc_deposit.txid, receiver_address: ltc_deposit.receiver_address, total_amount: 1, currency_iso: 'LTC'
        )
        expect(described_class.count).to eq 0
      end
    end
  end

  describe '.withdraw' do
    let!(:user) { create :user_with_funds }

    context 'when all data is valid' do
      it 'withdraw btc funds' do
        described_class.withdraw(
          sender: user.assets.find_by('balance.currency_iso' => 'BTC'),
          receiver_address: Cryptocurrencies::Bitcoin.getnewaddress(:primary), amount: 0.01
        )
        expect(described_class.count).to eq 3
      end

      it 'withdraw ltc funds' do
        described_class.withdraw(
          sender: user.assets.find_by('balance.currency_iso' => 'LTC'),
          receiver_address: Cryptocurrencies::Litecoin.getnewaddress(:primary), amount: 0.01
        )
        expect(described_class.count).to eq 3
      end
    end

    context 'when funds are not enough' do
      it 'fail to withdraw funds' do
        described_class.withdraw(
          sender: user.assets.find_by('balance.currency_iso' => 'BTC'),
          receiver_address: Cryptocurrencies::Bitcoin.getnewaddress(:primary), amount: 1.1
        )
        expect(described_class.count).to eq 2
      end
    end

    context 'when receiver address is invalid' do
      it 'fail to withdraw funds' do
        described_class.withdraw(
          sender: user.assets.find_by('balance.currency_iso' => 'BTC'),
          receiver_address: 'wrong address', amount: 0.01
        )
        expect(described_class.count).to eq 2
      end
    end

    context 'when currency is disabled' do
      it 'fail to withdraw funds' do
        Cryptocurrencies::Litecoin.settings[:enable] = false
        described_class.withdraw(
          sender: user.assets.find_by('balance.currency_iso' => 'LTC'),
          receiver_address: Cryptocurrencies::Litecoin.getnewaddress(:primary), amount: 0.01
        )
        expect(described_class.count).to eq 2
      end
    end
  end

  describe '.transfer' do
    let!(:user) { create :user_with_funds }
    let!(:other_user) { create :user }

    context 'when all data is valid' do
      it 'transfer btc funds' do
        described_class.transfer(
          sender: user.assets.find_by('balance.currency_iso' => 'BTC'),
          receiver: other_user.assets.find_by('balance.currency_iso' => 'BTC'),
          amount: 0.01
        )
        expect(described_class.count).to eq 3
      end

      it 'transfer ltc funds' do
        described_class.transfer(
          sender: user.assets.find_by('balance.currency_iso' => 'LTC'),
          receiver: other_user.assets.find_by('balance.currency_iso' => 'LTC'),
          amount: 0.01
        )
        expect(described_class.count).to eq 3
      end
    end

    context 'when funds are not enough' do
      it 'fail to transfer funds' do
        described_class.transfer(
          sender: user.assets.find_by('balance.currency_iso' => 'BTC'),
          receiver: other_user.assets.find_by('balance.currency_iso' => 'BTC'),
          amount: 1.1
        )
        expect(described_class.count).to eq 2
      end
    end

    context 'when receiver currency is invalid' do
      it 'fail to transfer funds' do
        described_class.transfer(
          sender: user.assets.find_by('balance.currency_iso' => 'BTC'),
          receiver: other_user.assets.find_by('balance.currency_iso' => 'LTC'),
          amount: 0.01
        )
        expect(described_class.count).to eq 2
      end
    end

    context 'when currency is disabled' do
      it 'fail to transfer funds' do
        Cryptocurrencies::Litecoin.settings[:enable] = false
        described_class.transfer(
          sender: user.assets.find_by('balance.currency_iso' => 'LTC'),
          receiver: other_user.assets.find_by('balance.currency_iso' => 'LTC'),
          amount: 0.01
        )
        expect(described_class.count).to eq 2
      end
    end
  end
end
