# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User::BlockedUser, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'associations' do
    it { is_expected.to be_embedded_in :user }
    it { is_expected.to belong_to(:blocked_user).of_type(User) }
  end

  describe '.count' do
    let(:user) { create :user }

    context 'when there is no blocked users' do
      it 'counts zero' do
        expect(user.blocked_users.count).to eq 0
      end
    end

    context 'when an user is blocked' do
      let(:user) { create :user_with_one_blocked_user }

      it 'counts one' do
        expect(user.blocked_users.count).to eq 1
      end
    end
  end
end
