# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User::Profile, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it { is_expected.to have_field(:display_name).of_type(String).with_default_value_of(nil) }
    it { is_expected.to have_field(:first_name).of_type(String).with_default_value_of(nil) }
    it { is_expected.to have_field(:last_name).of_type(String).with_default_value_of(nil) }
    it { is_expected.to have_field(:birthdate).of_type(Time).with_default_value_of(nil) }
    it { is_expected.to have_field(:country).of_type(ISO3166::Country).with_default_value_of(nil) }
    it { is_expected.to have_field(:bio).of_type(String).with_default_value_of(nil) }
  end

  describe 'associations' do
    it { is_expected.to be_embedded_in :user }
  end

  describe 'validations' do
    it { is_expected.to validate_length_of(:display_name).with_maximum(128) }
    it { is_expected.to validate_length_of(:first_name).with_maximum(256) }
    it { is_expected.to validate_length_of(:last_name).with_maximum(256) }
  end
end
