# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'matchers' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe 'fields' do
    it do
      expect(described_class).to(
        have_fields(:username, :email, :email_verification_token, :password_digest).of_type(String)
      )
    end

    it { is_expected.to have_field(:email_verified_at).of_type(Time) }
  end

  describe 'associations' do
    it { is_expected.to embed_many(:blocked_users).of_type(User::BlockedUser) }
    it { is_expected.to embed_one(:profile).of_type(User::Profile).with_autobuild }
    it { is_expected.to have_many(:assets).with_dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:talkers).with_dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:otps).with_dependent(:restrict_with_exception) }

    it { is_expected.to have_many(:reported_users).as_inverse_of(:user).with_dependent(:restrict_with_exception) }

    it do
      expect(described_class).to(
        have_many(:reports_received).of_type(ReportedUser).as_inverse_of(:reported_user)
        .with_dependent(:restrict_with_exception)
      )
    end
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :username }
    it { is_expected.to validate_length_of(:username).within(5..20) }
    it { is_expected.to validate_format_of(:username).to_allow('valid_username').not_to_allow('invalid username') }
    it { is_expected.to validate_exclusion_of(:username).to_not_allow('talkntrade') }
    it { is_expected.to validate_uniqueness_of(:username).case_insensitive }
    it { is_expected.to validate_presence_of(:email).on(:update) }
    it { is_expected.to validate_length_of(:email).within(5..256) }
    it { is_expected.to validate_format_of(:email).to_allow('valid@email.tld').not_to_allow('invalid email') }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
    it { is_expected.to validate_confirmation_of :password }
  end

  describe '.for_user' do
    let(:user) { create :user }

    context 'when there is no other users' do
      it 'counts zero' do
        expect(described_class.for_user(user).count).to eq 0
      end
    end

    context 'when there is another user' do
      let!(:another_user) { create :user }

      it 'counts one' do
        expect(described_class.for_user(user).count).to eq 1
      end

      it 'counts zero if the other user is blocked' do
        user.blocked_users.create blocked_user: another_user
        expect(described_class.for_user(user).count).to eq 0
      end

      it 'counts zero if the first user is blocked' do
        another_user.blocked_users.create blocked_user: user
        expect(described_class.for_user(user).count).to eq 0
      end

      it 'counts two if include current' do
        expect(described_class.for_user(user, exclude_current: false).count).to eq 2
      end
    end
  end

  describe '#find_enabled_talk' do
    let(:talk) { create :talk }
    let(:user) { talk.talkers.first.user }

    context 'when talk_id is valid' do
      it 'returns the talk' do
        expect(user.find_enabled_talk(talk.id)).to eq talk
      end
    end

    context 'when talk_id is invalid' do
      let(:talk1) { create :talk }

      it 'returns nil' do
        expect(user.find_enabled_talk(talk1.id)).to be_nil
      end
    end

    context 'when talk is disabled' do
      it 'returns nil' do
        talk.disable
        expect(user.find_enabled_talk(talk.id)).to be_nil
      end
    end
  end

  describe '#find_talk_with' do
    let(:talk) { create :talk }
    let(:user) { talk.talkers.first.user }
    let(:user1) { talk.talkers.last.user }

    context 'when the user is valid' do
      it 'returns the talk' do
        expect(user.find_talk_with(user1)).to eq talk
      end
    end

    context 'when the user is invalid' do
      let(:talk1) { create :talk }
      let(:user2) { talk1.talkers.first.user }

      it 'returns nil' do
        expect(user.find_talk_with(user2)).to be_nil
      end
    end

    context 'when talk is disabled' do
      it 'returns the talk' do
        talk.disable
        expect(user.find_talk_with(user1)).to eq talk
      end
    end
  end

  describe '#find_enabled_talk_with' do
    let(:talk) { create :talk }
    let(:user) { talk.talkers.first.user }
    let(:user1) { talk.talkers.last.user }

    context 'when talk is disabled' do
      it 'returns nil' do
        talk.disable
        expect(user.find_enabled_talk_with(user1)).to be_nil
      end
    end
  end
end
