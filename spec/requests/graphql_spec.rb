# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GraphQL', type: :request do
  describe 'POST graphql' do
    it 'responds with an introspection' do
      post '/graphql', params: {
        query: 'query IntrospectionQuery { __schema { queryType { name } mutationType { name } } }'
      }

      expect(response.content_type).to include('application/json')
      expect(response).to have_http_status(:success)
    end
  end
end
