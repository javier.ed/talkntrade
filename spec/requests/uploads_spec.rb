# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Uploads', type: :request do
  describe 'GET uploads/users/:id/avatar_image' do
    let(:user) { create :user_with_full_profile }
    let(:device) { create :device }
    let(:device_token) { JWT.encode({ app_id: device.client_app.id.to_s }, device.secret_key) }

    context 'when device is authenticated' do
      it 'responds successfully' do
        get "/uploads/users/#{user.id}/avatar_image",
            headers: { 'X-Device-Id' => device.id.to_s, 'X-Device-Token' => device_token }

        expect(response).to have_http_status(:success)
      end
    end

    context 'when device is not authenticated' do
      it 'responds with error 401' do
        get "/uploads/users/#{user.id}/avatar_image"

        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'when the user is invalid' do
      it 'responds with error 404' do
        get '/uploads/users/wrong_id/avatar_image',
            headers: { 'X-Device-Id' => device.id.to_s, 'X-Device-Token' => device_token }

        expect(response).to have_http_status(:not_found)
      end
    end

    context 'when there is no avatar image' do
      let(:user) { create :user }

      it 'responds with error 404' do
        get "/uploads/users/#{user.id}/avatar_image",
            headers: { 'X-Device-Id' => device.id.to_s, 'X-Device-Token' => device_token }

        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
