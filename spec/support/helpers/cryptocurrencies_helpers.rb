# frozen_string_literal: true

module CryptocurrenciesHelpers
  def stub_getnewaddress(url)
    stub_rpc(url: url, method: 'getnewaddress', result: random_string(35))
  end

  def stub_getaddressinfo(url)
    stub_rpc(url: url, method: 'getaddressinfo', result: { 'ismine' => false })
  end

  def stub_getwalletinfo(url)
    stub_rpc(url: url, method: 'getwalletinfo', result: { 'txcount' => 0, 'balance' => 2 })
  end

  def stub_sendtoaddress(url)
    stub_rpc(url: url, method: 'sendtoaddress', result: random_string(64))
  end

  def stub_listtransactions(url)
    stub_rpc(url: url, method: 'listtransactions', result: [])
  end

  def stub_rpc(url:, method:, result:)
    stub_request(:post, url)
      .with(body: hash_including(method: method))
      .to_return(body: { result: result }.to_json)
  end

  def random_string(length)
    Array.new(length) { (Array('a'..'z') + Array(0..9)).sample }.join
  end
end
