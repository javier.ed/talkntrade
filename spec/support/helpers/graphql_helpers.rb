# frozen_string_literal: true

module GraphqlHelpers
  def custom_context(current_user: nil)
    current_device = create :device
    current_session = current_device.sessions.create!(user: current_user) if current_user
    OpenStruct.new(
      current_device: current_device,
      current_session: current_session,
      current_user: current_user,
      request: OpenStruct.new(remote_ip: '127.0.0.1')
    )
  end
end
