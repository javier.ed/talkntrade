# frozen_string_literal: true

module GraphqlMatchers
  class BaseMatcher
    def initialize(*expected)
      @expected = expected.collect { |k| k.to_s.camelize(:lower) }
    end

    def of_type(type)
      @type = type
      self
    end

    def matches?(match)
      @match = match.is_a?(Class) ? match : match.class
      @errors = []
    end
  end

  class HaveGraphqlArgumentMatcher < BaseMatcher
    def required
      @required = true
      self
    end

    def matches?(match)
      super match

      @expected.each do |expected|
        if @match.arguments.key? expected
          error = ''

          error += validate_type(expected) || ''

          @errors << error unless error.empty?
        else
          @errors.push "no argument named #{expected.inspect}"
        end
      end

      @errors.empty?
    end

    def failure_message_for_should
      "Expected #{@match.inspect} to #{description}, got #{@errors.to_sentence}"
    end

    def failure_message_for_should_not
      "Expected #{@match.inspect} to not #{description}, got #{@match.inspect} to #{description}"
    end

    alias failure_message failure_message_for_should
    alias failure_message_when_negated failure_message_for_should_not

    def description
      desc = "have #{@expected.size > 1 ? 'arguments' : 'argument'} named #{@expected.collect(&:inspect).to_sentence}"
      desc += " of type #{@type.inspect}#{'!' if @required}" if @type
      desc
    end

    private

    def validate_type(expected)
      return unless @type

      type = if @type.is_a? Class
               @type&.to_graphql&.to_s
             elsif @type.is_a? Array
               "[#{@type.map { |k| "#{k.to_graphql}!" }.join(',')}]"
             else
               @type.to_s
             end

      type += '!' if @required

      actual_type = @match.arguments[expected].to_graphql.type.to_s

      " of type #{actual_type}" if actual_type != type
    end
  end

  class HaveGraphqlFieldMatcher < BaseMatcher
    def with_mutation(mutation)
      @mutation = mutation
      self
    end

    def allow_null
      @null = true
      self
    end

    def matches?(match)
      super match

      @expected.each do |expected|
        expected = expected.to_s.camelize(:lower)

        if @match.fields.key? expected
          error = ''

          error += validate_type(expected) || ''

          error += validate_mutation(expected) || ''

          @errors << error unless error.empty?
        else
          @errors << "no field named #{expected.inspect}"
        end
      end

      @errors.empty?
    end

    def failure_message_for_should
      "Expected #{@match.inspect} to #{description}, got #{@errors.to_sentence}"
    end

    def failure_message_for_should_not
      "Expected #{@match.inspect} to not #{description}, got #{@match.inspect} to #{description}"
    end

    alias failure_message failure_message_for_should
    alias failure_message_when_negated failure_message_for_should_not

    def description
      desc = "have #{@expected.size > 1 ? 'fields' : 'field'} named #{@expected.collect(&:inspect).to_sentence}"
      desc += " of type #{@type.inspect}#{'!' unless @null}" if @type
      desc += " with mutation #{@mutation.inspect}" if @mutation
      desc
    end

    private

    def validate_type(expected)
      return unless @type

      type = if @type.is_a? Class
               @type&.to_graphql&.to_s
             elsif @type.is_a? Array
               "[#{@type.map { |k| "#{k.to_graphql}!" }.join(',')}]"
             else
               @type.to_s
             end

      type += '!' unless @null

      actual_type = @match.fields[expected].to_graphql.type.to_s

      " of type #{actual_type}" if actual_type != type
    end

    def validate_mutation(expected)
      return unless @mutation

      mutation = @mutation.is_a?(Class) ? @mutation&.name : @mutation.to_s

      actual_mutation = @match.fields[expected].mutation.name

      " with mutation #{actual_mutation}" if actual_mutation != mutation
    end
  end

  def graphql_argument?(*expected)
    HaveGraphqlArgumentMatcher.new(*expected)
  end

  def graphql_field?(*expected)
    HaveGraphqlFieldMatcher.new(*expected)
  end

  def graphql_global_id_field?(expected)
    graphql_field?(expected).of_type(GraphQL::Types::ID)
  end

  alias have_graphql_argument graphql_argument?
  alias have_graphql_arguments graphql_argument?
  alias have_graphql_field graphql_field?
  alias have_graphql_fields graphql_field?
  alias have_graphql_global_id_field graphql_global_id_field?
end
