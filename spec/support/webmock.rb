# frozen_string_literal: true

require 'webmock/rspec'
require './spec/support/helpers/cryptocurrencies_helpers'

RSpec.configure do |config|
  config.include CryptocurrenciesHelpers

  config.before do
    Cryptocurrencies::Bitcoin.settings[:enable] = true
    stub_getnewaddress Cryptocurrencies::Bitcoin.settings[:rpc_url]
    stub_getaddressinfo Cryptocurrencies::Bitcoin.settings[:rpc_url]
    stub_getwalletinfo Cryptocurrencies::Bitcoin.settings[:rpc_url]
    stub_sendtoaddress Cryptocurrencies::Bitcoin.settings[:rpc_url]

    Cryptocurrencies::Litecoin.settings[:enable] = true
    stub_getnewaddress Cryptocurrencies::Litecoin.settings[:rpc_url]
    stub_getaddressinfo Cryptocurrencies::Litecoin.settings[:rpc_url]
    stub_getwalletinfo Cryptocurrencies::Litecoin.settings[:rpc_url]
    stub_sendtoaddress Cryptocurrencies::Litecoin.settings[:rpc_url]
  end
end
